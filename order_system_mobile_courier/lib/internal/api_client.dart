import 'package:order_system_api_client/order_system_api_client.dart';

import '../data/globals.dart' as globals;

class MobileApiClient extends ApiClient {
  MobileApiClient()
      : super(Uri.parse(globals.orderSystemApiAddress),
            onBeforeRequest: (ApiRequest request) {
          if (globals.authToken != null)
            return request.change(
                headers: {}
                  ..addAll(request.headers)
                  ..addAll({'authorization': globals.authToken}));
          return request;
        }, onAfterResponse: (ApiResponse response) {
          if (response.headers.containsKey('authorization'))
            globals.authToken = response.headers['authorization'];
          return response;
        });
}
