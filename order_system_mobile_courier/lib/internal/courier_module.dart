import 'package:order_system_mobile_courier/domain/bloc/field_forms/password_field_bloc.dart';
import 'package:order_system_mobile_courier/domain/bloc/field_forms/text_field_bloc.dart';
import 'package:order_system_mobile_courier/domain/bloc/login_form/login_form_bloc.dart';
import 'package:order_system_mobile_courier/domain/bloc/sign_up_form/sign_up_form_bloc.dart';

class UserModule {
  static TextFieldBloc textFieldBloc() {
    return TextFieldBloc(TextFieldState());
  }

  static PasswordFieldBloc passwordFieldBloc() {
    return PasswordFieldBloc(PasswordFieldState());
  }

  static LoginFormBloc loginFormBloc() {
    return LoginFormBloc(textFieldBloc(), passwordFieldBloc());
  }

  static SignUpFormBloc signUpFormBloc() {
    return SignUpFormBloc(
        textFieldBloc(), textFieldBloc(), textFieldBloc(), passwordFieldBloc());
  }
}
