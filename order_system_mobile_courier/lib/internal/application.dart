import 'package:flutter/material.dart';
import 'courier_module.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_mobile_courier/presentation/login/login_page.dart';
import 'package:order_system_mobile_courier/presentation/sign_up/sign_up_page.dart';
import 'package:order_system_mobile_courier/presentation/list_orders/list_orders_page.dart';
import 'package:order_system_mobile_courier/domain/bloc/list_orders_widget/list_orders_widget_bloc.dart';

class CourierApp extends StatefulWidget {
  @override
  _CourierAppState createState() => _CourierAppState();
}

class _CourierAppState extends State<CourierApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Order System Courier',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: BlocProvider(
        create: (context) => UserModule.loginFormBloc(),
        child: LoginPage(),
      ),
      routes: {
        '/sign_up': (context) => BlocProvider(
              create: (context) => UserModule.signUpFormBloc(),
              child: SignUpPage(),
            ),
        '/orders_list': (context) => BlocProvider(
              create: (context) =>
                  ListOrdersWidgetBloc(LoadingListOrdersWidgetState),
              child: NewOrdersPage(),
            ),
      },
    );
  }
}
