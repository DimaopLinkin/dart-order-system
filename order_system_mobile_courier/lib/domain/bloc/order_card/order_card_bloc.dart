import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_mobile_courier/data/globals.dart' as globals;
import 'package:order_system_api_client/order_system_api_client.dart';
import 'package:order_system_mobile_courier/internal/api_client.dart';
import 'package:order_system_models/order_system_models.dart';

class OrderCardBloc extends Bloc<OrderCardEvent, OrderCardState> {
  OrderCardBloc(OrderCardState initialState) : super(InitialOrderCardState());

  @override
  Stream<OrderCardState> mapEventToState(OrderCardEvent event) async* {
    if (event is TryAcceptOrderCardEvent) {
      yield await _mapAcceptToState(event);
    }
  }

  Future<OrderCardState> _mapAcceptToState(
      TryAcceptOrderCardEvent event) async {
    try {
      OrdersClient ordersClient = OrdersClient(MobileApiClient());
      event.order.courier = globals.currentUser;
      event.order.status = 'Ожидает подтверждения рестораном';
      var order = await ordersClient.update(event.order);
      return SuccessAcceptOrderCardState(order);
    } on Exception catch (e) {
      print('Accept Failed');
      print(e);
      return ErrorAcceptOrderCardState();
    }
  }
}

@immutable
abstract class OrderCardEvent {}

class TryAcceptOrderCardEvent extends OrderCardEvent {
  final Order order;
  TryAcceptOrderCardEvent(this.order);
}

@immutable
abstract class OrderCardState {}

class InitialOrderCardState extends OrderCardState {}

class SuccessAcceptOrderCardState extends OrderCardState {
  final Order order;
  SuccessAcceptOrderCardState(this.order);
}

class ErrorAcceptOrderCardState extends OrderCardState {}
