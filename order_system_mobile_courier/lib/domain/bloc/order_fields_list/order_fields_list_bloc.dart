import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_api_client/order_system_api_client.dart';
import 'package:order_system_mobile_courier/internal/api_client.dart';
import 'package:order_system_models/order_system_models.dart';

class OrderFieldsListBloc
    extends Bloc<OrderFieldsListEvent, OrderFieldsListState> {
  OrderFieldsListBloc(OrderFieldsListState initialState) : super(initialState);

  @override
  Stream<OrderFieldsListState> mapEventToState(
      OrderFieldsListEvent event) async* {
    if (event is RefreshOrderFieldsListEvent) {
      yield LoadingOrderFieldsListState();
      yield await _mapRefreshToState(event);
    }
    if (event is SubmitOrderFieldsListEvent) {
      yield LoadingOrderFieldsListState();
      yield await _mapSubmitToState(event);
    }
    if (event is PickUpOrderFieldsListEvent) {
      yield LoadingOrderFieldsListState();
      yield await _mapPickUpToState(event);
    }
  }

  Future<OrderFieldsListState> _mapRefreshToState(
      RefreshOrderFieldsListEvent event) async {
    try {
      Order order =
          await OrdersIdClient(MobileApiClient()).read(event.order.id);
      return LoadedOrderFieldsListState(order);
    } on Exception catch (e) {
      return ErrorOrderFieldsListState(e);
    }
  }

  Future<OrderFieldsListState> _mapSubmitToState(
      SubmitOrderFieldsListEvent event) async {
    try {
      Order order = event.order;
      order.status = 'Доставлен';
      order = await OrdersClient(MobileApiClient()).update(event.order);
      return LoadedOrderFieldsListState(order);
    } on Exception catch (e) {
      return ErrorOrderFieldsListState(e);
    }
  }

  Future<OrderFieldsListState> _mapPickUpToState(
      PickUpOrderFieldsListEvent event) async {
    try {
      Order order = event.order;
      order.status = 'Доставляется';
      order = await OrdersIdClient(MobileApiClient()).update(event.order);
      return LoadedOrderFieldsListState(order);
    } on Exception catch (e) {
      return ErrorOrderFieldsListState(e);
    }
  }
}

@immutable
abstract class OrderFieldsListEvent {}

class RefreshOrderFieldsListEvent extends OrderFieldsListEvent {
  final Order order;
  RefreshOrderFieldsListEvent(this.order);
}

class PickUpOrderFieldsListEvent extends OrderFieldsListEvent {
  final Order order;
  PickUpOrderFieldsListEvent(this.order);
}

class SubmitOrderFieldsListEvent extends OrderFieldsListEvent {
  final Order order;
  SubmitOrderFieldsListEvent(this.order);
}

@immutable
abstract class OrderFieldsListState {}

class LoadingOrderFieldsListState extends OrderFieldsListState {}

class LoadedOrderFieldsListState extends OrderFieldsListState {
  final Order order;
  LoadedOrderFieldsListState(this.order);
}

class ErrorOrderFieldsListState extends OrderFieldsListState {
  final dynamic error;
  ErrorOrderFieldsListState(this.error);
}
