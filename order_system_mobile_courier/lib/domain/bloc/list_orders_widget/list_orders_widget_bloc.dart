import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_api_client/order_system_api_client.dart';
import 'package:order_system_mobile_courier/internal/api_client.dart';
import 'package:order_system_models/order_system_models.dart';

class ListOrdersWidgetBloc
    extends Bloc<ListOrdersWidgetEvent, ListOrdersWidgetState> {
  ListOrdersWidgetBloc(initialState) : super(LoadingListOrdersWidgetState());

  @override
  Stream<ListOrdersWidgetState> mapEventToState(
      ListOrdersWidgetEvent event) async* {
    if (event is RefreshListOrdersWidgetEvent) {
      yield LoadingListOrdersWidgetState();
      yield await _mapRefreshToState(event);
    }
  }

  Future<ListOrdersWidgetState> _mapRefreshToState(
      RefreshListOrdersWidgetEvent event) async {
    try {
      List<Order> orderList =
          await OrdersNewCourierClient(MobileApiClient()).read({});
      return LoadedListOrdersWidgetState(orderList);
    } on Exception catch (e) {
      return ErrorListOrdersWidgetState(e);
    }
  }
}

@immutable
abstract class ListOrdersWidgetEvent {}

class RefreshListOrdersWidgetEvent extends ListOrdersWidgetEvent {}

@immutable
abstract class ListOrdersWidgetState {}

class LoadingListOrdersWidgetState extends ListOrdersWidgetState {}

class LoadedListOrdersWidgetState extends ListOrdersWidgetState {
  final listOrders;
  LoadedListOrdersWidgetState(this.listOrders);
}

class ErrorListOrdersWidgetState extends ListOrdersWidgetState {
  final dynamic error;
  ErrorListOrdersWidgetState(this.error);
}
