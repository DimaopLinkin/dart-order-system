import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_mobile_courier/presentation/components/order_card_widget.dart';
import 'package:order_system_models/order_system_models.dart';
import 'package:order_system_mobile_courier/domain/bloc/list_orders_widget/list_orders_widget_bloc.dart';
import 'package:order_system_mobile_courier/presentation/components/loading_widget.dart';

class ListOrdersWidget extends StatefulWidget {
  @override
  _ListOrdersWidgetState createState() => _ListOrdersWidgetState();
}

class _ListOrdersWidgetState extends State<ListOrdersWidget> {
  @override
  void initState() {
    BlocProvider.of<ListOrdersWidgetBloc>(context)
        .add(RefreshListOrdersWidgetEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ListOrdersWidgetBloc, ListOrdersWidgetState>(
      listener: (context, state) {
        if (state is ErrorListOrdersWidgetState) {
          print(state.error);
        }
      },
      child: Column(
        children: [
          Expanded(
              child: RefreshIndicator(
            onRefresh: () async {
              BlocProvider.of<ListOrdersWidgetBloc>(context)
                  .add(RefreshListOrdersWidgetEvent());
            },
            child: BlocBuilder<ListOrdersWidgetBloc, ListOrdersWidgetState>(
              builder: (context, state) {
                if (state is LoadedListOrdersWidgetState) {
                  return ListView.builder(
                    itemCount: state.listOrders.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        child: _buildListTile(state.listOrders[index]),
                      );
                    },
                  );
                } else {
                  return LoadingWidget();
                }
              },
            ),
          ))
        ],
      ),
    );
  }

  ListTile _buildListTile(Order order) {
    return ListTile(
      title: OrderCardWidget(order: order),
    );
  }
}
