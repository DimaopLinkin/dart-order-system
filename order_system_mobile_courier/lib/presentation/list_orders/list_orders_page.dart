import 'package:flutter/material.dart';
import 'package:order_system_mobile_courier/presentation/menu_drawer/menu_drawer_widget.dart';
import 'package:order_system_mobile_courier/presentation/list_orders/list_odrers_widget/list_orders_widget.dart';

class NewOrdersPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Orders'),
        centerTitle: true,
      ),
      drawer: MenuDrawerWidget(),
      body: ListOrdersWidget(),
    );
  }
}
