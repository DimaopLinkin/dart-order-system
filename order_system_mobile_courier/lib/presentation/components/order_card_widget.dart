import 'package:flutter/material.dart';
import 'package:order_system_models/order_system_models.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_mobile_courier/domain/bloc/order_card/order_card_bloc.dart';
import 'package:order_system_mobile_courier/presentation/current_order/current_order_page.dart';

class OrderCardWidget extends StatelessWidget {
  final Order order;
  const OrderCardWidget({Key key, this.order}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<OrderCardBloc>(
      create: (context) => OrderCardBloc(InitialOrderCardState()),
      child: SizedBox(
        height: 280,
        child: Column(
          children: [
            SizedBox(
              height: 60,
              child: Column(
                children: [
                  SizedBox(
                    width: double.infinity,
                    height: 59,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Order number',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(order.id.toString()),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 1,
                    child: Container(
                      color: Colors.deepPurple[400],
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 170,
              child: Column(
                children: [
                  SizedBox(
                    height: 169,
                    child: Row(
                      children: [
                        Stack(
                          alignment: Alignment.center,
                          children: [
                            Container(
                              width: 124,
                              height: 124,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(60),
                                color: Colors.grey[300],
                              ),
                            ),
                            Text(
                              'Avatar',
                              style: TextStyle(fontSize: 24),
                            ),
                          ],
                        ),
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  order.restaurant.name,
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text(
                                  order.restaurant.information,
                                  style: TextStyle(
                                    fontSize: 12,
                                  ),
                                ),
                                Text(
                                  order.restaurant.address,
                                  style: TextStyle(
                                    fontSize: 12,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 1,
                    child: Container(
                      color: Colors.deepPurple[400],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 50,
              child: Column(
                children: [
                  SizedBox(
                    height: 48,
                    child: BlocListener<OrderCardBloc, OrderCardState>(
                      listener: (context, state) {
                        if (state is SuccessAcceptOrderCardState) {
                          _routeToOrder(context, state.order);
                        }
                      },
                      child: BlocBuilder<OrderCardBloc, OrderCardState>(
                        builder: (context, state) {
                          return InkWell(
                            onTap: () {
                              context
                                  .read<OrderCardBloc>()
                                  .add(TryAcceptOrderCardEvent(order));
                            },
                            child: Text(
                              'Accept',
                              style: TextStyle(
                                fontSize: 36,
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 2,
                    child: Container(
                      color: Colors.deepPurple[400],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void _routeToOrder(BuildContext context, Order order) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CurrentOrderPage(order: order)));
  }
}
