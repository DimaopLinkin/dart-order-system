import 'package:flutter/material.dart';
import 'package:order_system_models/order_system_models.dart';

ListTile buildListTile(InOrderDish inOrderDish) {
  return ListTile(
    title: SizedBox(
      height: 100,
      child: Column(
        children: [
          SizedBox(
            height: 99,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      width: 70,
                      height: 70,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(60),
                        color: Colors.grey[300],
                      ),
                    ),
                    Text(
                      'Avatar',
                      style: TextStyle(fontSize: 12),
                    ),
                  ],
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          inOrderDish.dish.name,
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          inOrderDish.dish.information,
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(inOrderDish.count.toString()),
                    Text(inOrderDish.dish.price.toString()),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 1,
            width: double.infinity,
            child: Container(
              color: Colors.deepPurple[400],
            ),
          ),
        ],
      ),
    ),
  );
}
