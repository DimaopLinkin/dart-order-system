import 'package:flutter/material.dart';
import 'package:order_system_mobile_courier/presentation/components/in_order_dish_tile.dart';
import 'package:order_system_models/order_system_models.dart';

class ListDishesPage extends StatelessWidget {
  final Order order;
  const ListDishesPage({Key key, this.order}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dish list'),
        centerTitle: true,
      ),
      body: ListView.builder(
        itemCount: order.dishList.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            child: buildListTile(order.dishList[index]),
          );
        },
      ),
    );
  }
}
