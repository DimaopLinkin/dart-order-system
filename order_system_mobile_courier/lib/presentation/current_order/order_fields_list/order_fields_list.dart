import 'package:flutter/material.dart';
import 'package:order_system_mobile_courier/presentation/components/button_widget.dart';
import 'package:order_system_mobile_courier/presentation/components/text_widget.dart';
import 'package:order_system_mobile_courier/presentation/list_dishes/lish_dishes_page.dart';
import 'package:order_system_models/order_system_models.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_mobile_courier/domain/bloc/order_fields_list/order_fields_list_bloc.dart';

import '../../components/loading_widget.dart';

class OrderFieldsList extends StatefulWidget {
  final Order order;
  OrderFieldsList(this.order);

  @override
  _OrderFieldsListState createState() => _OrderFieldsListState();
}

class _OrderFieldsListState extends State<OrderFieldsList> {
  @override
  void initState() {
    BlocProvider.of<OrderFieldsListBloc>(context)
        .add(RefreshOrderFieldsListEvent(widget.order));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<OrderFieldsListBloc, OrderFieldsListState>(
      listener: (context, state) {
        if (state is ErrorOrderFieldsListState) {
          print(state.error);
        }
      },
      child: Column(
        children: [
          Expanded(
            child: RefreshIndicator(
              onRefresh: () async {
                BlocProvider.of<OrderFieldsListBloc>(context)
                    .add(RefreshOrderFieldsListEvent(widget.order));
              },
              child: BlocBuilder<OrderFieldsListBloc, OrderFieldsListState>(
                builder: (context, state) {
                  if (state is LoadedOrderFieldsListState) {
                    if (state.order.status == 'Доставлен') {
                      return ListViewOrder(order: state.order);
                    }
                    if (state.order.status == 'Доставляется') {
                      return ListViewOrdering(order: state.order);
                    }
                    if (state.order.status == 'Готов') {
                      return ListViewReady(order: state.order);
                    }
                    if (state.order.status != 'Готов') {
                      return ListViewWaiting(order: state.order);
                    }
                  }
                  return LoadingWidget();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ListViewOrder extends StatelessWidget {
  final Order order;
  const ListViewOrder({
    Key key,
    @required this.order,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ListView(
          children: [
            TextWidget(
              label: 'Order number',
              text: order.id.toString(),
            ),
            TextWidget(
              label: 'Client',
              text: order.client.name,
            ),
            TextWidget(
              label: 'Client address',
              text: order.client.address,
            ),
            TextWidget(
              label: 'Client phone',
              text: order.client.phone,
            ),
            TextWidget(
              label: 'Order price',
              text: order.sum.toString() + ' р',
            ),
            TextWidget(
              label: 'Order status',
              text: order.status,
            ),
            InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ListDishesPage(order: order)));
              },
              child: TextWidget(
                label: 'Dish list',
                text: 'Click to open',
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 25),
          child: Container(
            alignment: Alignment.bottomCenter,
            child: Button('Take new order', () {
              Navigator.pushNamedAndRemoveUntil(
                  context, '/orders_list', (route) => false);
            }),
          ),
        ),
      ],
    );
  }
}

class ListViewOrdering extends StatelessWidget {
  final Order order;
  const ListViewOrdering({
    Key key,
    @required this.order,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ListView(
          children: [
            TextWidget(
              label: 'Order number',
              text: order.id.toString(),
            ),
            TextWidget(
              label: 'Client',
              text: order.client.name,
            ),
            TextWidget(
              label: 'Client address',
              text: order.client.address,
            ),
            TextWidget(
              label: 'Client phone',
              text: order.client.phone,
            ),
            TextWidget(
              label: 'Order price',
              text: order.sum.toString() + ' р',
            ),
            TextWidget(
              label: 'Order status',
              text: order.status,
            ),
            InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ListDishesPage(order: order)));
              },
              child: TextWidget(
                label: 'Dish list',
                text: 'Click to open',
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 25),
          child: Container(
            alignment: Alignment.bottomCenter,
            child: Button('Submit order', () {
              BlocProvider.of<OrderFieldsListBloc>(context)
                  .add(SubmitOrderFieldsListEvent(order));
            }),
          ),
        ),
      ],
    );
  }
}

class ListViewReady extends StatelessWidget {
  final Order order;
  const ListViewReady({
    Key key,
    @required this.order,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ListView(
          children: [
            TextWidget(
              label: 'Order number',
              text: order.id.toString(),
            ),
            TextWidget(
              label: 'Restaurant',
              text: order.restaurant.name,
            ),
            TextWidget(
              label: 'Restaurant address',
              text: order.restaurant.address,
            ),
            TextWidget(
              label: 'Order price',
              text: order.sum.toString() + ' р',
            ),
            TextWidget(
              label: 'Order status',
              text: order.status,
            ),
            InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ListDishesPage(order: order)));
              },
              child: TextWidget(
                label: 'Dish list',
                text: 'Click to open',
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 25),
          child: Container(
            alignment: Alignment.bottomCenter,
            child: Button('Pick up', () {
              BlocProvider.of<OrderFieldsListBloc>(context)
                  .add(PickUpOrderFieldsListEvent(order));
            }),
          ),
        ),
      ],
    );
  }
}

class ListViewWaiting extends StatelessWidget {
  final Order order;
  const ListViewWaiting({
    Key key,
    @required this.order,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        TextWidget(
          label: 'Order number',
          text: order.id.toString(),
        ),
        TextWidget(
          label: 'Restaurant',
          text: order.restaurant.name,
        ),
        TextWidget(
          label: 'Restaurant address',
          text: order.restaurant.address,
        ),
        TextWidget(
          label: 'Order price',
          text: order.sum.toString() + ' р',
        ),
        TextWidget(
          label: 'Order status',
          text: order.status,
        ),
        InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ListDishesPage(order: order)));
          },
          child: TextWidget(
            label: 'Dish list',
            text: 'Click to open',
          ),
        ),
      ],
    );
  }
}
