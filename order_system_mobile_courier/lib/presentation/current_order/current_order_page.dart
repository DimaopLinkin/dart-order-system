import 'package:flutter/material.dart';
import 'package:order_system_mobile_courier/presentation/menu_drawer/menu_drawer_widget.dart';
import 'package:order_system_models/order_system_models.dart';
import 'package:order_system_mobile_courier/presentation/current_order/order_fields_list/order_fields_list.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_mobile_courier/domain/bloc/order_fields_list/order_fields_list_bloc.dart';

class CurrentOrderPage extends StatefulWidget {
  final Order order;
  const CurrentOrderPage({Key key, this.order}) : super(key: key);
  @override
  _CurrentOrderPageState createState() => _CurrentOrderPageState();
}

class _CurrentOrderPageState extends State<CurrentOrderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Order'),
        centerTitle: true,
      ),
      drawer: MenuDrawerWidget(),
      body: BlocProvider(
        create: (context) => OrderFieldsListBloc(LoadingOrderFieldsListState()),
        child: OrderFieldsList(widget.order),
      ),
    );
  }
}
