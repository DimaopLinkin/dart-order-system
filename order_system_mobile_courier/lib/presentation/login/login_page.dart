import 'package:flutter/material.dart';
import 'package:order_system_mobile_courier/domain/bloc/login_form/login_form_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_mobile_courier/presentation/components/button_widget.dart';
import 'package:order_system_mobile_courier/presentation/components/form/name_widget.dart';
import 'package:order_system_mobile_courier/presentation/components/form/password_widget.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginFormBloc, LoginFormState>(
      listener: (context, state) {
        if (state is SuccessLoginFormState) {
          _onLoginSuccess();
        }
        if (state is FailedLoginFormState) {
          _onLoginFailed(context);
        }
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text('Authorization'),
          centerTitle: true,
        ),
        body: GestureDetector(
          onTap: () {
            BlocProvider.of<LoginFormBloc>(context)
                .add(UnFocusLoginFormEvent());
          },
          child: BlocBuilder<LoginFormBloc, LoginFormState>(
            builder: (context, loginFormState) {
              return Container(
                padding: EdgeInsets.all(20.0),
                child: Form(
                  key: this._formKey,
                  child: Column(
                    children: [
                      NameWidget('Login ',
                          BlocProvider.of<LoginFormBloc>(context).loginBloc),
                      PasswordWidget(
                          BlocProvider.of<LoginFormBloc>(context).passwordBloc),
                      SizedBox(
                        height: 40,
                      ),
                      Button('Login', _onLogin),
                      SizedBox(
                        height: 40,
                      ),
                      Button('Sign Up', _onSignUp),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  void _onLogin() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    BlocProvider.of<LoginFormBloc>(context).add(TryLoginFormEvent());
  }

  void _onSignUp() {
    Navigator.pushNamed(context, '/sign_up');
  }

  void _onLoginSuccess() {
    Navigator.pushReplacementNamed(context, '/orders_list');
  }

  void _onLoginFailed(BuildContext context) {
    final snackBar = SnackBar(content: Text('Login failed'));
    Scaffold.of(context).showSnackBar(snackBar);
  }
}
