import 'package:flutter/material.dart';
import 'package:order_system_mobile_courier/data/globals.dart' as globals;
import 'package:order_system_mobile_courier/internal/courier_module.dart';
import 'package:order_system_mobile_courier/presentation/components/button_widget.dart';
import 'package:order_system_mobile_courier/presentation/login/login_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MenuDrawerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          SizedBox(
            height: 294,
            child: Container(
              color: Colors.deepPurple[400],
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 150, 10, 50),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      globals.currentUser.name,
                      style: TextStyle(
                        fontSize: 24,
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      globals.currentUser.phone,
                      style: TextStyle(
                        fontSize: 24,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          buildButton('Order history', () {}), // TODO: СДЕЛАТЬ ПЕРЕХОДЫ
          buildButton('Logout', () {
            globals.currentUser = null;
            globals.authToken = null;
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (context) => BlocProvider(
                          create: (context) => UserModule.loginFormBloc(),
                          child: LoginPage(),
                        )),
                (route) => false);
          }),
        ],
      ),
    );
  }

  Widget buildButton(String label, Function onTap) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 0),
      child: Button(label, onTap),
    );
  }
}
