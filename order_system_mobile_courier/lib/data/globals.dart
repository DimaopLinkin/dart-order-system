library order_system_mobile_courier.globals;

import 'package:order_system_models/order_system_models.dart';

const String host = '192.168.0.2';
const String webSocketAddress = 'ws://$host:3333/ws';
const String orderSystemApiAddress = 'http://$host:3333';

String authToken;
Courier currentUser;
