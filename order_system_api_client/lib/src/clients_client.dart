import 'dart:async';

import 'package:order_system_models/order_system_models.dart';
import 'package:rest_api_client/rest_api_client.dart';

class ClientsClient extends ResourceClient<Client> {
  ClientsClient(ApiClient apiClient) : super('clients', apiClient);

  Future<Client> login(String login, String password) async {
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.post,
        resourcePath: '$resourcePath/login',
        body: {'login': login, 'password': password}));
    print(response.body.runtimeType);
    return processResponse(response);
  }

  @override
  Client createModel(Map<String, dynamic> json) {
    print(json);
    return Client.fromJson(json);
  }
}
