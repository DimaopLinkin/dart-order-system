import 'package:order_system_api_client/order_system_api_client.dart';
import 'package:order_system_models/order_system_models.dart';

class OrdersIdClient extends ResourceClient<Order> {
  OrdersIdClient(ApiClient apiClient) : super('orders', apiClient);

  @override
  Future<Order> read(orderId, {Map<String, String> headers = const {}}) async {
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.get,
        resourcePath: '$resourcePath/${orderId.json}',
        headers: headers));
    return processResponse(response);
  }

  @override
  Order createModel(Map<String, dynamic> json) => Order.fromJson(json);
}
