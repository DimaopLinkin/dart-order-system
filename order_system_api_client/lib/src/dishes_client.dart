import 'package:order_system_models/order_system_models.dart';
import 'package:rest_api_client/rest_api_client.dart';

class DishesClient extends ResourceClient<Dish> {
  DishesClient(ApiClient apiClient) : super('dishes', apiClient);

  @override
  Dish createModel(Map<String, dynamic> json) => Dish.fromJson(json);
}
