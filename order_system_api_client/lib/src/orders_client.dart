import 'dart:async';

import 'package:order_system_models/order_system_models.dart';
import 'package:rest_api_client/rest_api_client.dart';

class OrdersClient extends ResourceClient<Order> {
  OrdersClient(ApiClient apiClient) : super('orders', apiClient);

  @override
  Future delete(dynamic obj, {Map<String, String> headers = const {}}) {
    throw (UnsupportedError('Delete is not supported'));
  }

  @override
  Order createModel(Map<String, dynamic> json) => Order.fromJson(json);
}
