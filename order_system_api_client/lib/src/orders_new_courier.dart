import 'package:order_system_api_client/order_system_api_client.dart';
import 'package:order_system_models/order_system_models.dart';

class OrdersNewCourierClient extends ResourceClient<Order> {
  OrdersNewCourierClient(ApiClient apiClient)
      : super('orders/couriers', apiClient);

  @override
  Future<List<Order>> read(_, {Map<String, String> headers = const {}}) async {
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.get,
        resourcePath: '$resourcePath',
        headers: headers));
    return processResponse(response);
  }

  @override
  Order createModel(Map<String, dynamic> json) => Order.fromJson(json);
}
