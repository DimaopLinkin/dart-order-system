import 'package:order_system_models/order_system_models.dart';
import 'package:rest_api_client/rest_api_client.dart';

class RestaurantsDishesClient extends ResourceClient<Dish> {
  RestaurantsDishesClient(ApiClient apiClient)
      : super('restaurants', apiClient);

  @override
  Future<List<Dish>> read(restaurantId,
      {Map<String, String> headers = const {}}) async {
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.get,
        resourcePath: '$resourcePath/${restaurantId.json}/dishes',
        headers: headers));
    return processResponse(response);
  }

  @override
  Dish createModel(Map<String, dynamic> json) => Dish.fromJson(json);
}
