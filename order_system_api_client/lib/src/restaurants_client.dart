import 'dart:async';

import 'package:order_system_models/order_system_models.dart';
import 'package:rest_api_client/rest_api_client.dart';

class RestaurantClient extends ResourceClient<Restaurant> {
  RestaurantClient(ApiClient apiClient) : super('restaurants', apiClient);

  Future<Restaurant> login(String login, String password) async {
    final response = await apiClient.send(ApiRequest(
        method: HttpMethod.post,
        resourcePath: '$resourcePath/login',
        body: {'login': login, 'password': password}));
    print(response.body.runtimeType);
    return processResponse(response);
  }

  @override
  Restaurant createModel(Map<String, dynamic> json) {
    print(json);
    return Restaurant.fromJson(json);
  }
}
