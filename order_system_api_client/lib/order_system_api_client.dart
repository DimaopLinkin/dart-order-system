library order_system_api_client;

export 'package:rest_api_client/rest_api_client.dart';
export 'src/clients_client.dart';
export 'src/couriers_client.dart';
export 'src/dishes_client.dart';
export 'src/orders_client.dart';
export 'src/restaurants_client.dart';
export 'src/restaurants_dihes_client.dart';
export 'src/orders_id_client.dart';
export 'src/orders_new_courier.dart';
