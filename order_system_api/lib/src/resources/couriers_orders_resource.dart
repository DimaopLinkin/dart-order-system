import 'package:order_system_api/collections.dart';
import 'package:order_system_models/order_system_models.dart';
import 'package:rest_api_server/annotations.dart';
import 'package:rest_api_server/service_registry.dart';
import 'package:mongo_dart/mongo_dart.dart' as mongo;

/// Courier's orders resource
@Resource(path: 'couriers/{courierIdStr}/orders')
class CouriersOrdersResource {
  OrdersCollection ordersCollection = locateService<OrdersCollection>();

  /// Reads Courier's orders from database
  @Get()
  Future<List<Order>> read(String courierIdStr, Map context) async {
    final orders =
        await ordersCollection.find(mongo.where.eq('courier', courierIdStr));
    return orders.toList();
  }
}
