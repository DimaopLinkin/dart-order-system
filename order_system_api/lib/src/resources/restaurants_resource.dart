import 'dart:convert';
import 'dart:io';

import 'package:mongo_dart/mongo_dart.dart' as mongo;
import 'package:order_system_api/collections.dart';
import 'package:order_system_models/order_system_models.dart';
import 'package:rest_api_server/annotations.dart';
import 'package:rest_api_server/http_exception.dart';
import 'package:rest_api_server/service_registry.dart';
import 'package:shelf/shelf.dart' as shelf;

/// Restaurants resource
@Resource(path: 'restaurants')
class RestaurantsResource {
  RestaurantsCollection restaurantsCollection =
      locateService<RestaurantsCollection>();

  RestaurantsResource();

  /// Makes login
  @Post(path: 'login')
  Future<shelf.Response> login(Map requestBody) async {
    final login = requestBody['login'];
    final password = requestBody['password'];
    if (login == null || password == null) {
      throw (BadRequestException({}, 'Login or password not provided'));
    }
    final found = await restaurantsCollection
        .find(mongo.where.eq('login', login).eq('password', password))
        .toList();
    if (found.isEmpty) {
      throw UnauthorizedException({}, 'Incorrect login or password');
    }
    final restaurant = found.single;
    return shelf.Response.ok(
      json.encode(restaurant.json),
      headers: {'Content-Type': ContentType.json.toString()},
      context: {'subject': restaurant.id.json, 'payload': restaurant.json},
    );
  }

  /// Creates new restaurant
  @Post()
  Future<Restaurant> create(Map requestBody, Map context) async {
    final newRestaurant = Restaurant.fromJson(requestBody);
    final currentUser = Restaurant.fromJson(context['payload']);
    if (currentUser != null) {
      throw (ForbiddenException(
          {}, 'You are not allowed to create restaurants'));
    }
    final query = mongo.where.eq('name', newRestaurant.name);
    final found = await restaurantsCollection.find(query).toList();
    if (found.length != 0) {
      throw (BadRequestException({}, 'Restaurant already exists'));
    }
    return restaurantsCollection.insert(newRestaurant);
  }

  /// Reads restaurants from database
  @Get()
  Future<List<Restaurant>> read(String name, Map context) {
    final currentUser = Restaurant.fromJson(context['payload']);
    if (currentUser == null) throw (FormatException());
    final query = mongo.where;
    if (name != null) {
      query.match('name', name, caseInsensitive: true);
    }
    return restaurantsCollection.find(query).toList();
  }
}
