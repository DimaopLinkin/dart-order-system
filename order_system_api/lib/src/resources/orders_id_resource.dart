import 'package:order_system_api/collections.dart';
import 'package:order_system_models/order_system_models.dart';
import 'package:rest_api_server/annotations.dart';
import 'package:rest_api_server/service_registry.dart';

@Resource(path: 'orders/{orderIdStr}')
class OrdersIdResource {
  OrdersCollection ordersCollection = locateService<OrdersCollection>();

  @Get()
  Future<Order> read(String orderIdStr, Map context) async {
    final order = await ordersCollection.findOne(OrderId(orderIdStr));
    return order;
  }
}
