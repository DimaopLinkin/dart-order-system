import 'dart:convert';

import 'package:mongo_dart/mongo_dart.dart' as mongo;
import 'package:order_system_api/collections.dart';
import 'package:order_system_api/helpers.dart';
import 'package:order_system_models/order_system_models.dart';
import 'package:rest_api_server/annotations.dart';
import 'package:rest_api_server/http_exception.dart';
import 'package:rest_api_server/service_registry.dart';

/// Orders resource
@Resource(path: 'orders')
class OrdersResource {
  final OrdersCollection ordersCollection = locateService<OrdersCollection>();
  final WsChannels wsChannels = locateService<WsChannels>();
  OrdersResource();

  /// Creates new order
  @Post()
  Future<Order> create(Map requestBody, Map context) async {
    final currentUser = Client.fromJson(context['payload']);
    if (currentUser == null) {
      throw (ForbiddenException({}, 'You are not allowed to create orders'));
    }
    requestBody['status'] = 'Ожидает подтверждения курьером';
    requestBody['createdAt'] = DateTime.now();
    requestBody['updatedAt'] = DateTime.now();
    final newOrder = Order.fromJson(requestBody);
    wsChannels.channels.forEach((wsChannel) {
      wsChannel.sink.add(json.encode(newOrder.json, toEncodable: toEncodable));
    });
    return ordersCollection.insert(newOrder);
  }

  /// Reads orders from database
  @Get()
  Future<List<Order>> read(String name, Map context) {
    final query = mongo.where;
    if (name != null) {
      query.match('name', name, caseInsensitive: true);
    }
    return ordersCollection.find(query).toList();
  }

  /// Updates order
  @Patch()
  Future<Order> update(Map requestBody, Map context) async {
    requestBody['updatedAt'] = DateTime.now();
    final order = Order.fromJson(requestBody);
    final currentClient = Client.fromJson(context['payload']);
    final currentCourier = Courier.fromJson(context['payload']);
    final currentRestaurant = Restaurant.fromJson(context['payload']);
    if (order.client == currentClient ||
        order.courier == currentCourier ||
        order.courier == null ||
        order.restaurant == currentRestaurant)
      return ordersCollection.update(order);
    else
      throw ForbiddenException({}, 'You are not allowed to update this order');
    // TODO: Уточнить про UPDATE в wsChannels.
  }
}
