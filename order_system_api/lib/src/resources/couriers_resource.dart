import 'dart:convert';
import 'dart:io';

import 'package:mongo_dart/mongo_dart.dart' as mongo;
import 'package:order_system_api/collections.dart';
import 'package:order_system_models/order_system_models.dart';
import 'package:rest_api_server/annotations.dart';
import 'package:rest_api_server/http_exception.dart';
import 'package:rest_api_server/service_registry.dart';
import 'package:shelf/shelf.dart' as shelf;

/// Couriers resource
@Resource(path: 'couriers')
class CouriersResource {
  CouriersCollection couriersCollection = locateService<CouriersCollection>();

  CouriersResource();

  /// Makes login
  @Post(path: 'login')
  Future<shelf.Response> login(Map requestBody) async {
    final login = requestBody['login'];
    final password = requestBody['password'];
    if (login == null || password == null) {
      throw (BadRequestException({}, 'Login or password not provided'));
    }
    final found = await couriersCollection
        .find(mongo.where.eq('login', login).eq('password', password))
        .toList();
    if (found.isEmpty) {
      throw UnauthorizedException({}, 'Incorrect login or password');
    }
    final courier = found.single;
    return shelf.Response.ok(
      json.encode(courier.json),
      headers: {'Content-Type': ContentType.json.toString()},
      context: {'subject': courier.id.json, 'payload': courier.json},
    );
  }

  /// Creates new courier
  @Post()
  Future<Courier> create(Map requestBody, Map context) async {
    final newCourier = Courier.fromJson(requestBody);
    final currentUser = Courier.fromJson(context['payload']);
    if (currentUser != null) {
      throw (ForbiddenException({}, 'You are not allowed to create couriers'));
    }
    final query = mongo.where.eq('name', newCourier.name);
    final found = await couriersCollection.find(query).toList();
    if (found.length != 0) {
      throw (BadRequestException({}, 'Courier already exists'));
    }
    return couriersCollection.insert(newCourier);
  }

  /// Reads couriers from database
  @Get()
  Future<List<Courier>> read(String name, Map context) {
    final currentUser = Courier.fromJson(context['payload']);
    if (currentUser == null) throw (FormatException());
    final query = mongo.where;
    if (name != null) {
      query.match('name', name, caseInsensitive: true);
    }
    return couriersCollection.find(query).toList();
  }
}
