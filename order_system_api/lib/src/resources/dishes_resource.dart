import 'package:mongo_dart/mongo_dart.dart' as mongo;
import 'package:order_system_api/collections.dart';
import 'package:order_system_models/order_system_models.dart';
import 'package:rest_api_server/annotations.dart';
import 'package:rest_api_server/http_exception.dart';
import 'package:rest_api_server/service_registry.dart';

/// Dishes resource
@Resource(path: 'dishes')
class DishesResource {
  final DishesCollection dishesCollection = locateService<DishesCollection>();

  DishesResource();

  /// Creates new dish
  @Post()
  Future<Dish> create(Map requestBody, Map context) async {
    final newDish = Dish.fromJson(requestBody);
    final currentUser = Restaurant.fromJson(context['payload']);
    if (currentUser == null) {
      throw (ForbiddenException({}, 'You are not allowed to create dishes'));
    }
    final query = mongo.where.eq('name', newDish.name);
    final found = await dishesCollection.find(query).toList();
    if (found.length != 0) {
      throw (BadRequestException({}, 'Dish already exists'));
    }
    return dishesCollection.insert(newDish);
  }

  /// Reads dishes from database
  @Get()
  Future<List<Dish>> read(String name, Map context) {
    final query = mongo.where;
    if (name != null) {
      query.match('name', name, caseInsensitive: true);
    }
    return dishesCollection.find(query).toList();
  }
}
