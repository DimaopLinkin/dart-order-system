import 'package:order_system_api/collections.dart';
import 'package:order_system_models/order_system_models.dart';
import 'package:rest_api_server/annotations.dart';
import 'package:rest_api_server/service_registry.dart';
import 'package:mongo_dart/mongo_dart.dart' as mongo;

/// Client's orders resource
@Resource(path: 'clients/{clientIdStr}/orders')
class ClientsOrdersResource {
  OrdersCollection ordersCollection = locateService<OrdersCollection>();

  /// Reads Client's orders from database
  @Get()
  Future<List<Order>> read(String clientIdStr, Map context) async {
    final orders =
        await ordersCollection.find(mongo.where.eq('client', clientIdStr));
    return orders.toList();
  }
}
