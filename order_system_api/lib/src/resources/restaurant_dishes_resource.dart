import 'package:order_system_api/collections.dart';
import 'package:order_system_models/order_system_models.dart';
import 'package:rest_api_server/annotations.dart';
import 'package:rest_api_server/service_registry.dart';
import 'package:mongo_dart/mongo_dart.dart' as mongo;

/// Restaurant's dishes resource
@Resource(path: 'restaurants/{restaurantIdStr}/dishes')
class RestaurantsDishesResource {
  DishesCollection dishesCollection = locateService<DishesCollection>();

  /// Reads Restaurant's dishes from database
  @Get()
  Future<List<Dish>> read(String restaurantIdStr, Map context) async {
    final dishes = await dishesCollection
        .find(mongo.where.eq('restaurant', restaurantIdStr));
    return dishes.toList();
  }
}
