import 'dart:convert';
import 'dart:io';

import 'package:mongo_dart/mongo_dart.dart' as mongo;
import 'package:order_system_api/collections.dart';
import 'package:order_system_models/order_system_models.dart';
import 'package:rest_api_server/annotations.dart';
import 'package:rest_api_server/http_exception.dart';
import 'package:rest_api_server/service_registry.dart';
import 'package:shelf/shelf.dart' as shelf;

/// Clients resource
@Resource(path: 'clients')
class ClientsResource {
  ClientsCollection clientsCollection = locateService<ClientsCollection>();

  ClientsResource();

  /// Makes login
  @Post(path: 'login')
  Future<shelf.Response> login(Map requestBody) async {
    final login = requestBody['login'];
    final password = requestBody['password'];
    if (login == null || password == null) {
      throw (BadRequestException({}, 'Login or password not provided'));
    }
    final found = await clientsCollection
        .find(mongo.where.eq('login', login).eq('password', password))
        .toList();
    if (found.isEmpty) {
      throw UnauthorizedException({}, 'Incorrect login or password');
    }
    final client = found.single;
    return shelf.Response.ok(
      json.encode(client.json),
      headers: {'Content-Type': ContentType.json.toString()},
      context: {'subject': client.id.json, 'payload': client.json},
    );
  }

  /// Creates new client
  @Post()
  Future<Client> create(Map requestBody, Map context) async {
    final newClient = Client.fromJson(requestBody);
    final currentUser = Client.fromJson(context['payload']);
    if (currentUser != null) {
      throw (ForbiddenException({}, 'You are not allowed to create clients'));
    }
    final query = mongo.where.eq('login', newClient.login);
    final found = await clientsCollection.find(query).toList();
    if (found.length != 0) {
      throw (BadRequestException({}, 'Client already exists'));
    }
    return clientsCollection.insert(newClient);
  }

  /// Reads clients from database
  @Get()
  Future<List<Client>> read(String login, Map context) {
    final currentUser = Client.fromJson(context['payload']);
    if (currentUser == null) throw (FormatException());
    final query = mongo.where;
    if (login != null) {
      query.match('login', login, caseInsensitive: true);
    }
    return clientsCollection.find(query).toList();
  }

  /// Updates user
  @Patch()
  Future<Client> update(Map requestBody, Map context) async {
    final client = Client.fromJson(requestBody);
    final currentClient = Client.fromJson(context['payload']);
    if (client.id != currentClient.id)
      throw ForbiddenException({}, 'You are not allowed to update this client');
    return clientsCollection.update(client);
  }
}
