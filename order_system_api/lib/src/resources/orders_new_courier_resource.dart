import 'package:mongo_dart/mongo_dart.dart' as mongo;
import 'package:order_system_api/collections.dart';
import 'package:order_system_models/order_system_models.dart';
import 'package:rest_api_server/annotations.dart';
import 'package:rest_api_server/service_registry.dart';

/// Orders resource
@Resource(path: 'orders/couriers')
class OrdersNewCourierResource {
  final OrdersCollection ordersCollection = locateService<OrdersCollection>();

  /// Reads orders from database
  @Get()
  Future<List<Order>> read(Map context) {
    final query = mongo.where;

    query.match('status', 'Ожидает подтверждения курьером',
        caseInsensitive: true);

    return ordersCollection.find(query).toList();
  }
}
