import 'package:order_system_models/order_system_models.dart';
import 'package:mongo_dart/mongo_dart.dart' as mongo;
import 'package:rest_api_server/mongo_collection.dart';

/// Couriers collection
///
/// Implements basic operations with MongoDb couriers collection

class CouriersCollection extends MongoCollection<Courier, CourierId> {
  CouriersCollection(mongo.DbCollection collection) : super(collection);

  @override
  Courier createModel(Map<String, dynamic> data) => Courier.fromJson(data);

  @override
  List<Map<String, dynamic>> buildPipeline(mongo.SelectorBuilder query) {
    final pipeline = super.buildPipeline(query);
    pipeline
      ..firstWhere((stage) => stage.keys.first == r'$project')[r'$project']
          .addAll({'login': false, 'password': false});
    return pipeline;
  }
}
