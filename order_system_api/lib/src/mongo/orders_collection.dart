import 'package:order_system_models/order_system_models.dart';
import 'package:mongo_dart/mongo_dart.dart' as mongo;
import 'package:rest_api_server/mongo_collection.dart';

/// Orders collection
///
/// Implements basic operations with MongoDb orders collection

class OrdersCollection extends MongoCollection<Order, OrderId> {
  OrdersCollection(mongo.DbCollection collection) : super(collection);

  @override
  Order createModel(Map<String, dynamic> data) => Order.fromJson(data);
}
