import 'package:order_system_models/order_system_models.dart';
import 'package:mongo_dart/mongo_dart.dart' as mongo;
import 'package:rest_api_server/mongo_collection.dart';

/// Dishes collection
///
/// Implements basic operations with MongoDb dishes collection

class DishesCollection extends MongoCollection<Dish, DishId> {
  DishesCollection(mongo.DbCollection collection) : super(collection);

  @override
  Dish createModel(Map<String, dynamic> data) => Dish.fromJson(data);
}
