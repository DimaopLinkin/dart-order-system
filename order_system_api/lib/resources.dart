library order_system_api;

export 'src/resources/clients_resource.dart';
export 'src/resources/couriers_resource.dart';
export 'src/resources/dishes_resource.dart';
export 'src/resources/orders_resource.dart';
export 'src/resources/restaurants_resource.dart';
export 'src/resources/clients_orders_resource.dart';
export 'src/resources/orders_id_resource.dart';
export 'src/resources/orders_new_courier_resource.dart';
export 'src/resources/restaurant_dishes_resource.dart';
export 'src/resources/couriers_orders_resource.dart';
