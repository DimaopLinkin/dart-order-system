import 'dart:convert';
import 'package:shelf/shelf.dart';
import 'package:rest_api_server/src/route.dart';
import 'package:order_system_api/src/resources/clients_resource.dart';
import 'package:order_system_api/src/resources/couriers_resource.dart';
import 'package:order_system_api/src/resources/dishes_resource.dart';
import 'package:order_system_api/src/resources/orders_resource.dart';
import 'package:order_system_api/src/resources/restaurants_resource.dart';
import 'package:order_system_api/src/resources/web_socket_resource.dart';
import 'package:order_system_api/src/resources/restaurant_dishes_resource.dart';
import 'package:order_system_api/src/resources/orders_id_resource.dart';
import 'package:order_system_api/src/resources/clients_orders_resource.dart';
import 'package:order_system_api/src/resources/orders_new_courier_resource.dart';
import 'package:order_system_api/src/resources/couriers_orders_resource.dart';

final _clientsResource = ClientsResource();
final _couriersResource = CouriersResource();
final _dishesResource = DishesResource();
final _ordersResource = OrdersResource();
final _restaurantsResource = RestaurantsResource();
final _webSocketResource = WebSocketResource();
final _restaurantsDishesResource = RestaurantsDishesResource();
final _ordersIdResource = OrdersIdResource();
final _clientsOrdersResource = ClientsOrdersResource();
final _ordersNewCourierResource = OrdersNewCourierResource();
final _couriersOrdersResource = CouriersOrdersResource();
final routes = <Route>[
  Route('POST', 'clients/login', (Request request) async {
    final reqParameters = <String, String>{};
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result =
        _clientsResource.login(json.decode(await request.readAsString()));
    return makeResponseFrom(result);
  }),
  Route('POST', 'clients', (Request request) async {
    final reqParameters = <String, String>{};
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result = _clientsResource.create(
        json.decode(await request.readAsString()), request.context);
    return makeResponseFrom(result);
  }),
  Route('GET', 'clients', (Request request) async {
    final reqParameters = <String, String>{};
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result =
        _clientsResource.read(reqParameters['login'], request.context);
    return makeResponseFrom(result);
  }),
  Route('PATCH', 'clients', (Request request) async {
    final reqParameters = <String, String>{};
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result = _clientsResource.update(
        json.decode(await request.readAsString()), request.context);
    return makeResponseFrom(result);
  }),
  Route('POST', 'couriers/login', (Request request) async {
    final reqParameters = <String, String>{};
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result =
        _couriersResource.login(json.decode(await request.readAsString()));
    return makeResponseFrom(result);
  }),
  Route('POST', 'couriers', (Request request) async {
    final reqParameters = <String, String>{};
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result = _couriersResource.create(
        json.decode(await request.readAsString()), request.context);
    return makeResponseFrom(result);
  }),
  Route('GET', 'couriers', (Request request) async {
    final reqParameters = <String, String>{};
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result =
        _couriersResource.read(reqParameters['name'], request.context);
    return makeResponseFrom(result);
  }),
  Route('POST', 'dishes', (Request request) async {
    final reqParameters = <String, String>{};
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result = _dishesResource.create(
        json.decode(await request.readAsString()), request.context);
    return makeResponseFrom(result);
  }),
  Route('GET', 'dishes', (Request request) async {
    final reqParameters = <String, String>{};
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result = _dishesResource.read(reqParameters['name'], request.context);
    return makeResponseFrom(result);
  }),
  Route('POST', 'orders', (Request request) async {
    final reqParameters = <String, String>{};
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result = _ordersResource.create(
        json.decode(await request.readAsString()), request.context);
    return makeResponseFrom(result);
  }),
  Route('GET', 'orders', (Request request) async {
    final reqParameters = <String, String>{};
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result = _ordersResource.read(reqParameters['name'], request.context);
    return makeResponseFrom(result);
  }),
  Route('PATCH', 'orders', (Request request) async {
    final reqParameters = <String, String>{};
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result = _ordersResource.update(
        json.decode(await request.readAsString()), request.context);
    return makeResponseFrom(result);
  }),
  Route('POST', 'restaurants/login', (Request request) async {
    final reqParameters = <String, String>{};
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result =
        _restaurantsResource.login(json.decode(await request.readAsString()));
    return makeResponseFrom(result);
  }),
  Route('POST', 'restaurants', (Request request) async {
    final reqParameters = <String, String>{};
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result = _restaurantsResource.create(
        json.decode(await request.readAsString()), request.context);
    return makeResponseFrom(result);
  }),
  Route('GET', 'restaurants', (Request request) async {
    final reqParameters = <String, String>{};
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result =
        _restaurantsResource.read(reqParameters['name'], request.context);
    return makeResponseFrom(result);
  }),
  Route('GET', 'ws', (Request request) async {
    final reqParameters = <String, String>{};
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result =
        _webSocketResource.handleUpgradeRequest(request, request.context);
    return makeResponseFrom(result);
  }),
  Route('GET', 'restaurants/{restaurantIdStr}/dishes', (Request request) async {
    final offset = (List.from(request.requestedUri.pathSegments)
              ..removeWhere((segment) => segment.isEmpty))
            .length -
        3;
    final reqParameters = <String, String>{
      'restaurantIdStr': request.requestedUri.pathSegments[1 + offset]
    };
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result = _restaurantsDishesResource.read(
        reqParameters['restaurantIdStr'], request.context);
    return makeResponseFrom(result);
  }),
  Route('GET', 'orders/{orderIdStr}', (Request request) async {
    final offset = (List.from(request.requestedUri.pathSegments)
              ..removeWhere((segment) => segment.isEmpty))
            .length -
        2;
    final reqParameters = <String, String>{
      'orderIdStr': request.requestedUri.pathSegments[1 + offset]
    };
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result =
        _ordersIdResource.read(reqParameters['orderIdStr'], request.context);
    return makeResponseFrom(result);
  }),
  Route('GET', 'clients/{clientIdStr}/orders', (Request request) async {
    final offset = (List.from(request.requestedUri.pathSegments)
              ..removeWhere((segment) => segment.isEmpty))
            .length -
        3;
    final reqParameters = <String, String>{
      'clientIdStr': request.requestedUri.pathSegments[1 + offset]
    };
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result = _clientsOrdersResource.read(
        reqParameters['clientIdStr'], request.context);
    return makeResponseFrom(result);
  }),
  Route('GET', 'orders/couriers', (Request request) async {
    final reqParameters = <String, String>{};
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result = _ordersNewCourierResource.read(request.context);
    return makeResponseFrom(result);
  }),
  Route('GET', 'couriers/{courierIdStr}/orders', (Request request) async {
    final offset = (List.from(request.requestedUri.pathSegments)
              ..removeWhere((segment) => segment.isEmpty))
            .length -
        3;
    final reqParameters = <String, String>{
      'courierIdStr': request.requestedUri.pathSegments[1 + offset]
    };
    reqParameters.addAll(request.requestedUri.queryParameters);
    final result = _couriersOrdersResource.read(
        reqParameters['courierIdStr'], request.context);
    return makeResponseFrom(result);
  })
];
