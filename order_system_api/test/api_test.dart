import 'dart:convert';
import 'dart:math';

import 'package:test/test.dart';
import 'dart:io';

import 'package:order_system_api/collections.dart';
import 'package:order_system_api/helpers.dart';
import 'package:order_system_api/routes.g.dart' as generated;
import 'package:mongo_dart/mongo_dart.dart' as mongo;
import 'package:rest_api_server/api_server.dart';
import 'package:rest_api_server/auth_middleware.dart';
import 'package:rest_api_server/cors_headers_middleware.dart';
import 'package:rest_api_server/http_exception_middleware.dart';
import 'package:rest_api_server/service_registry.dart';
import 'package:shelf/shelf.dart' as shelf;

main() async {
  ApiServer server;

  final db = mongo.Db('mongodb://localhost:27017/order_system_test');
  await db.open();

  register<ClientsCollection>(
      ClientsCollection(mongo.DbCollection(db, 'clients')));
  register<DishesCollection>(
      DishesCollection(mongo.DbCollection(db, 'dishes')));
  register<CouriersCollection>(
      CouriersCollection(mongo.DbCollection(db, 'couriers')));
  register<RestaurantsCollection>(
      RestaurantsCollection(mongo.DbCollection(db, 'restaurants')));
  register<OrdersCollection>(
      OrdersCollection(mongo.DbCollection(db, 'orders')));
  register<Jwt>(Jwt(
      securityKey: 'secret key',
      issuer: 'Order System',
      maxAge: Duration(hours: 1)));
  register<WsChannels>(WsChannels());

  final router = Router(generated.routes);

  final loginPaths = {
    'POST': [
      '/clients/login',
      '/couriers/login',
      '/restaurants/login',
    ],
  };

  final excludePaths = {
    'POST': [
      ...loginPaths['POST'],
      '/clients',
      '/couriers',
      '/restaurants',
    ],
    'GET': ['/ws'],
  };

  setUpAll(() async {
    server = ApiServer(
        address: InternetAddress.loopbackIPv4,
        port: 3000 + Random().nextInt(1000),
        handler: shelf.Pipeline()
            .addMiddleware(CorsHeadersMiddleware({
              'Access-Control-Allow-Origin': '*',
              'Access-Control-Expose-Headers': 'Authorization, Content-Type',
              'Access-Control-Allow-Headers':
                  'Authorization, Origin, X-Requested-With, Content-Type, Accept, Content-Disposition',
              'Access-Control-Allow-Methods': 'GET, POST, PUT, PATCH, DELETE'
            }))
            .addMiddleware(HttpExceptionMiddleware())
            .addMiddleware(AuthMiddleware(
                loginPaths: loginPaths,
                exclude: excludePaths,
                jwt: locateService<Jwt>()))
            .addHandler(router.handler));

    await server.start();
  });

  tearDownAll(() async {
    await server.stop();
  });
  group('Клиент', () {
    test('Клиент уже создан (400)', () async {
      final request =
          await HttpClient().post(server.address.host, server.port, 'clients');
      request.headers.contentType =
          new ContentType("application", "json", charset: "utf-8");
      String map =
          '{"name": "Artem","phone": "71112223344","address": "pr. Stroiteley, d. 25, kv. 32","login": "Artem44","password": "NasNeDogonyat"}';
      request.write(map);
      final response = await request.close();
      final body = await response.transform(utf8.decoder).join();
      expect(response.statusCode, 400);
      expect(body, '{"status":400,"message":"Client already exists"}');
    });
    test('Авторизация клиента и запросы (200)', () async {
      final request = await HttpClient()
          .post(server.address.host, server.port, 'clients/login');
      request.headers.contentType =
          new ContentType("application", "json", charset: "utf-8");
      String map = '{"login": "Artem44","password": "NasNeDogonyat"}';
      request.write(map);
      final response = await request.close();
      final body = await response.transform(utf8.decoder).join();
      expect(response.statusCode, 200);
      expect(body,
          '{"id":"5fb6aa2fc8264438fdfef1ec","name":"Artem","phone":"71112223344","address":"pr. Stroiteley, d. 25, kv. 32"}');
      var jwt = response.headers['authorization'].toString();
      jwt = jwt.substring(1, jwt.length - 1);

      final request2 =
          await HttpClient().get(server.address.host, server.port, 'dishes');
      request2.headers.contentType =
          new ContentType("application", "json", charset: "utf-8");
      request2.headers.add('authorization', jwt);
      final response2 = await request2.close();
      final body2 = await response2.transform(utf8.decoder).join();
      expect(response2.statusCode, 200);
      expect(body2, '[]');

      final request3 = await HttpClient()
          .get(server.address.host, server.port, 'restaurants');
      request3.headers.contentType =
          new ContentType("application", "json", charset: "utf-8");
      request3.headers.add('authorization', jwt);
      final response3 = await request3.close();
      expect(response3.statusCode, 200);
    });
    test('Неверный ввод логина или пароля (401)', () async {
      final request = await HttpClient()
          .post(server.address.host, server.port, 'clients/login');
      request.headers.contentType =
          new ContentType("application", "json", charset: "utf-8");
      String map = '{"login": "Artem44","password": "NasDogonyat"}';
      request.write(map);
      final response = await request.close();
      expect(response.statusCode, 401);
    });
  });
  group('Ресторан', () {
    test('Ресторан уже создан (400)', () async {
      final request = await HttpClient()
          .post(server.address.host, server.port, 'restaurants');
      request.headers.contentType =
          new ContentType("application", "json", charset: "utf-8");
      String map =
          '{"name": "Макдональдс","information": "Бургерная из Америки","address": "пр. Ленинский, д. 1","login": "MacDonalds","password": "HeyYou123"}';
      request.write(map);
      final response = await request.close();
      final body = await response.transform(utf8.decoder).join();
      expect(response.statusCode, 400);
      expect(body, '{"status":400,"message":"Restaurant already exists"}');
    });
    test('Авторизация ресторана и запросы (200)', () async {
      final request = await HttpClient()
          .post(server.address.host, server.port, 'restaurants/login');
      request.headers.contentType =
          new ContentType("application", "json", charset: "utf-8");
      String map = '{"login": "MacDonalds","password": "HeyYou123"}';
      request.write(map);
      final response = await request.close();
      final body = await response.transform(utf8.decoder).join();
      expect(response.statusCode, 200);
      expect(body,
          '{"id":"5fb770ccc2bbe7478392caff","name":"Макдональдс","information":"Бургерная из Америки","address":"пр. Ленинский, д. 1"}');
      var jwt = response.headers['authorization'].toString();
      jwt = jwt.substring(1, jwt.length - 1);

      final request2 =
          await HttpClient().get(server.address.host, server.port, 'dishes');
      request2.headers.contentType =
          new ContentType("application", "json", charset: "utf-8");
      request2.headers.add('authorization', jwt);
      final response2 = await request2.close();
      final body2 = await response2.transform(utf8.decoder).join();
      expect(response2.statusCode, 200);
      expect(body2, '[]');
    });
    test('Неверный ввод логина или пароля (401)', () async {
      final request = await HttpClient()
          .post(server.address.host, server.port, 'restaurants/login');
      request.headers.contentType =
          new ContentType("application", "json", charset: "utf-8");
      String map = '{"login": "MacDonalds","password": "HeyYou"}';
      request.write(map);
      final response = await request.close();
      expect(response.statusCode, 401);
    });
  });
  group('Курьер', () {
    test('Курьер уже создан (400)', () async {
      final request =
          await HttpClient().post(server.address.host, server.port, 'couriers');
      request.headers.contentType =
          new ContentType("application", "json", charset: "utf-8");
      String map =
          '{"name":"Володя","phone":"79998887766","login":"Volodya7766","password":"VolodyaRullezz"}';
      request.write(map);
      final response = await request.close();
      final body = await response.transform(utf8.decoder).join();
      expect(response.statusCode, 400);
      expect(body, '{"status":400,"message":"Courier already exists"}');
    });
    test('Авторизация курьера и запросы (200)', () async {
      final request = await HttpClient()
          .post(server.address.host, server.port, 'couriers/login');
      request.headers.contentType =
          new ContentType("application", "json", charset: "utf-8");
      String map = '{"login":"Volodya7766","password":"VolodyaRullezz"}';
      request.write(map);
      final response = await request.close();
      final body = await response.transform(utf8.decoder).join();
      expect(response.statusCode, 200);
      expect(body,
          '{"id":"5fb774832c27673302cedc6b","name":"Володя","phone":"79998887766"}');
      var jwt = response.headers['authorization'].toString();
      jwt = jwt.substring(1, jwt.length - 1);

      final request2 =
          await HttpClient().get(server.address.host, server.port, 'dishes');
      request2.headers.contentType =
          new ContentType("application", "json", charset: "utf-8");
      request2.headers.add('authorization', jwt);
      final response2 = await request2.close();
      final body2 = await response2.transform(utf8.decoder).join();
      expect(response2.statusCode, 200);
      expect(body2, '[]');
    });
    test('Неверный ввод логина или пароля (401)', () async {
      final request = await HttpClient()
          .post(server.address.host, server.port, 'couriers/login');
      request.headers.contentType =
          new ContentType("application", "json", charset: "utf-8");
      String map = '{"login":"Volodya7766","password":"VolodyaRullezzZzZzZz"}';
      request.write(map);
      final response = await request.close();
      expect(response.statusCode, 401);
    });
  });
}
