import 'dart:io';

import 'package:order_system_api/collections.dart';
import 'package:order_system_api/helpers.dart';
import 'package:order_system_api/routes.g.dart' as generated;
import 'package:mongo_dart/mongo_dart.dart' as mongo;
import 'package:rest_api_server/api_server.dart';
import 'package:rest_api_server/auth_middleware.dart';
import 'package:rest_api_server/cors_headers_middleware.dart';
import 'package:rest_api_server/http_exception_middleware.dart';
import 'package:rest_api_server/service_registry.dart';
import 'package:shelf/shelf.dart' as shelf;

main() async {
  final db = mongo.Db('mongodb://localhost:27017/order_system');
  await db.open();

  register<ClientsCollection>(
      ClientsCollection(mongo.DbCollection(db, 'clients')));
  register<DishesCollection>(
      DishesCollection(mongo.DbCollection(db, 'dishes')));
  register<CouriersCollection>(
      CouriersCollection(mongo.DbCollection(db, 'couriers')));
  register<RestaurantsCollection>(
      RestaurantsCollection(mongo.DbCollection(db, 'restaurants')));
  register<OrdersCollection>(
      OrdersCollection(mongo.DbCollection(db, 'orders')));
  register<Jwt>(Jwt(
      securityKey: 'secret key',
      issuer: 'Order System',
      maxAge: Duration(hours: 1)));
  register<WsChannels>(WsChannels());

  final router = Router(generated.routes);

  final loginPaths = {
    'POST': [
      '/clients/login',
      '/couriers/login',
      '/restaurants/login',
    ],
  };

  final excludePaths = {
    'POST': [
      ...loginPaths['POST'],
      '/clients',
      '/couriers',
      '/restaurants',
    ],
    'GET': ['/ws'],
  };

  final server = ApiServer(
      address: InternetAddress.anyIPv4,
      port: 3333,
      handler: shelf.Pipeline()
          .addMiddleware(CorsHeadersMiddleware({
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Expose-Headers': 'Authorization, Content-Type',
            'Access-Control-Allow-Headers':
                'Authorization, Origin, X-Requested-With, Content-Type, Accept, Content-Disposition',
            'Access-Control-Allow-Methods': 'GET, POST, PUT, PATCH, DELETE'
          }))
          .addMiddleware(HttpExceptionMiddleware())
          .addMiddleware(AuthMiddleware(
              loginPaths: loginPaths,
              exclude: excludePaths,
              jwt: locateService<Jwt>()))
          .addHandler(router.handler));

  await server.start();
  router.printRoutes();
}
