# Тестовое задание "Система доставки" на языке Dart

## Этапы реализации

* Реализация пакета Models
* Реализация пакета Api
* Реализация клиентских пакетов:
  * Mobile_Client
  * Mobile_Courier
  * Web_Restaurant

## Диаграмма классов
## https://lucid.app/invitations/accept/6676ef0b-fa4c-4b64-8a30-d67fa14f75e7

## Диаграмма модели данных
## https://lucid.app/invitations/accept/20b33b8a-22f4-4af9-a3d6-ba30067795c9

## Прототип интерфейса
## https://www.figma.com/files/project/18295776/Test