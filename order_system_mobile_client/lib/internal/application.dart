import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_mobile_client/domain/bloc/list_restaurants_widget/list_restaurants_widget_bloc.dart';
import 'package:order_system_mobile_client/domain/order_system_component.dart';
import 'package:order_system_mobile_client/data/globals.dart' as globals;
import 'package:order_system_mobile_client/presentation/dishes/dihes_page.dart';
import 'package:order_system_mobile_client/presentation/login/login_page.dart';
import 'package:order_system_mobile_client/presentation/restaurants/restaurants_page.dart';
import 'package:order_system_mobile_client/presentation/sign_up/sign_up_page.dart';

import 'dependencies/client_module.dart';

class ClientApp extends StatefulWidget {
  final OrderSystemComponent _orderSystemComponent =
      OrderSystemComponent(globals.webSocketAddress);

  @override
  _ClientAppState createState() => _ClientAppState();
}

class _ClientAppState extends State<ClientApp> {
  @override
  void initState() {
    super.initState();
    widget._orderSystemComponent.connect();
  }

  @override
  void dispose() {
    widget._orderSystemComponent.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return OrderSystemComponentWidget(
      widget._orderSystemComponent,
      MaterialApp(
        title: 'Order System Client',
        theme: ThemeData(
          primarySwatch: Colors.deepPurple,
        ),
        home: BlocProvider(
          create: (context) => UserModule.loginFormBloc(),
          child: LoginPage(),
        ),
        routes: {
          '/sign_up': (context) => BlocProvider(
                create: (context) => UserModule.signUpFormBloc(),
                child: SignUpPage(),
              ),
          '/restaurant_list': (context) => BlocProvider(
                create: (context) => ListRestaurantsWidgetBloc(
                    LoadingListRestaurantsWidgetState),
                child: RestaurantsPage(),
              ),
          '/dish_list': (context) => DishesPage() // TODO: СПРОСИТЬ О ПАРАМЕТРЕ
        },
      ),
    );
  }
}
