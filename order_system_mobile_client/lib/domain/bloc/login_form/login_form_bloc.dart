import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:order_system_api_client/order_system_api_client.dart';
import 'package:order_system_mobile_client/data/globals.dart' as globals;
import 'package:order_system_mobile_client/domain/bloc/field_forms/password_field_bloc.dart';
import 'package:order_system_mobile_client/domain/bloc/field_forms/text_field_bloc.dart';
import 'package:order_system_mobile_client/internal/api_client.dart';

class LoginFormBloc extends Bloc<LoginFormEvent, LoginFormState> {
  final TextFieldBloc loginBloc;
  final PasswordFieldBloc passwordBloc;

  LoginFormBloc(this.loginBloc, this.passwordBloc)
      : super(InitLoginFormState());

  @override
  Future<void> close() {
    loginBloc.close();
    passwordBloc.close();
    return super.close();
  }

  @override
  Stream<LoginFormState> mapEventToState(LoginFormEvent event) async* {
    if (event is TryLoginFormEvent) {
      yield await _mapLoginToState(event);
    }
    if (event is UnFocusLoginFormEvent) {
      if (loginBloc.focusNode.hasFocus) {
        loginBloc.focusNode.unfocus();
      }
      if (passwordBloc.focusNode.hasFocus) {
        passwordBloc.focusNode.unfocus();
      }
    }
  }

  Future<LoginFormState> _mapLoginToState(TryLoginFormEvent event) async {
    try {
      ClientsClient clientsClient = ClientsClient(MobileApiClient());
      var client =
          await clientsClient.login(loginBloc.value, passwordBloc.value);
      globals.currentUser = client;
      return SuccessLoginFormState();
    } on Exception catch (e) {
      print('Login failed');
      print(e);
      return FailedLoginFormState();
    }
  }
}

@immutable
abstract class LoginFormEvent {}

class TryLoginFormEvent extends LoginFormEvent {}

class UnFocusLoginFormEvent extends LoginFormEvent {}

@immutable
abstract class LoginFormState {}

class InitLoginFormState extends LoginFormState {}

class SuccessLoginFormState extends LoginFormState {}

class FailedLoginFormState extends LoginFormState {}
