import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_api_client/order_system_api_client.dart';
import 'package:order_system_mobile_client/internal/api_client.dart';
import 'package:order_system_models/order_system_models.dart';

class ListRestaurantsWidgetBloc
    extends Bloc<ListRestaurantsWidgetEvent, ListRestaurantsWidgetState> {
  ListRestaurantsWidgetBloc(initialState)
      : super(LoadingListRestaurantsWidgetState());

  @override
  Stream<ListRestaurantsWidgetState> mapEventToState(
      ListRestaurantsWidgetEvent event) async* {
    if (event is RefreshListRestaurantsWidgetEvent) {
      yield LoadingListRestaurantsWidgetState();
      yield await _mapRefreshToState(event);
    }
  }

  Future<ListRestaurantsWidgetState> _mapRefreshToState(
      RefreshListRestaurantsWidgetEvent event) async {
    try {
      List<Restaurant> restaurantList =
          await RestaurantClient(MobileApiClient()).read({});
      return LoadedListRestaurantsWidgetState(restaurantList);
    } on Exception catch (e) {
      return ErrorListRestaurantsWidgetState(e);
    }
  }
}

@immutable
abstract class ListRestaurantsWidgetEvent {}

class RefreshListRestaurantsWidgetEvent extends ListRestaurantsWidgetEvent {}

@immutable
abstract class ListRestaurantsWidgetState {}

class LoadingListRestaurantsWidgetState extends ListRestaurantsWidgetState {}

class LoadedListRestaurantsWidgetState extends ListRestaurantsWidgetState {
  final listRestaurants;
  LoadedListRestaurantsWidgetState(this.listRestaurants);
}

class ErrorListRestaurantsWidgetState extends ListRestaurantsWidgetState {
  final dynamic error;
  ErrorListRestaurantsWidgetState(this.error);
}
