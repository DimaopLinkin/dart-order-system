import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_api_client/order_system_api_client.dart';
import 'package:order_system_mobile_client/internal/api_client.dart';
import 'package:order_system_models/order_system_models.dart';

class ListDishesWidgetBloc
    extends Bloc<ListDishesWidgetEvent, ListDishesWidgetState> {
  ListDishesWidgetBloc(initialState) : super(LoadingListDishesWidgetState());

  @override
  Stream<ListDishesWidgetState> mapEventToState(
      ListDishesWidgetEvent event) async* {
    if (event is RefreshListDishesWidgetEvent) {
      yield LoadingListDishesWidgetState();
      yield await _mapRefreshToState(event);
    }
  }

  Future<ListDishesWidgetState> _mapRefreshToState(
      RefreshListDishesWidgetEvent event) async {
    try {
      List<Dish> dishList = await RestaurantsDishesClient(MobileApiClient())
          .read(event.restaurantId);
      return LoadedListDishesWidgetState(dishList);
    } on Exception catch (e) {
      return ErrorListDishesWidgetState(e);
    }
  }
}

@immutable
abstract class ListDishesWidgetEvent {}

class RefreshListDishesWidgetEvent extends ListDishesWidgetEvent {
  final restaurantId;
  RefreshListDishesWidgetEvent(this.restaurantId);
}

@immutable
abstract class ListDishesWidgetState {}

class LoadingListDishesWidgetState extends ListDishesWidgetState {}

class LoadedListDishesWidgetState extends ListDishesWidgetState {
  final listDishes;
  LoadedListDishesWidgetState(this.listDishes);
}

class ErrorListDishesWidgetState extends ListDishesWidgetState {
  final dynamic error;
  ErrorListDishesWidgetState(this.error);
}
