import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_mobile_client/data/globals.dart' as globals;
import 'package:order_system_api_client/order_system_api_client.dart';
import 'package:order_system_mobile_client/internal/api_client.dart';
import 'package:order_system_models/order_system_models.dart';

class CreateOrderBloc extends Bloc<CreateOrderEvent, CreateOrderState> {
  CreateOrderBloc(CreateOrderState initialState)
      : super(InitCreateOrderState());

  @override
  Stream<CreateOrderState> mapEventToState(CreateOrderEvent event) async* {
    if (event is TrySubmitCreateOrderEvent) {
      yield await _mapSubmitToState(event);
    }
  }

  Future<CreateOrderState> _mapSubmitToState(
      TrySubmitCreateOrderEvent event) async {
    try {
      OrdersClient ordersClient = OrdersClient(MobileApiClient());
      var order = await ordersClient.create(Order(
          client: globals.currentUser,
          dishList: globals.cartList,
          restaurant: globals.currentRestaurant));
      globals.currentRestaurant = null;
      globals.cartList = [];
      print(order);
      return SubmitSuccessCreateOrderState(order);
    } on Exception catch (e) {
      print('Submit failed');
      print(e);
      return SubmitFailedCreateOrderState();
    }
  }
}

@immutable
abstract class CreateOrderEvent {}

class TrySubmitCreateOrderEvent extends CreateOrderEvent {}

@immutable
abstract class CreateOrderState {}

class InitCreateOrderState extends CreateOrderState {}

class SubmitSuccessCreateOrderState extends CreateOrderState {
  final Order order;
  SubmitSuccessCreateOrderState(this.order);
}

class SubmitFailedCreateOrderState extends CreateOrderState {}
