import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_mobile_client/data/globals.dart' as globals;
import 'package:order_system_models/order_system_models.dart';

class DishTileWidgetCubit extends Cubit<DishTileWidgetState> {
  Dish dish;
  DishTileWidgetCubit(this.dish) : super(DishTileWidgetState(counterValue: 0));

  void increment() {
    // TODO: Переделать без globals
    globals.cartList.remove(InOrderDish(dish: dish, count: state.counterValue));
    emit(DishTileWidgetState(counterValue: state.counterValue + 1));
    globals.cartList.add(InOrderDish(dish: dish, count: state.counterValue));
  }

  void decrement() {
    // TODO: Переделать без globals
    globals.cartList.remove(InOrderDish(dish: dish, count: state.counterValue));
    emit(DishTileWidgetState(counterValue: state.counterValue - 1));
    globals.cartList.add(InOrderDish(dish: dish, count: state.counterValue));
  }
}

class DishTileWidgetState {
  int counterValue;

  DishTileWidgetState({
    @required this.counterValue,
  });
}
