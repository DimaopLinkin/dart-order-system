import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:order_system_api_client/order_system_api_client.dart';
import 'package:order_system_mobile_client/domain/bloc/field_forms/password_field_bloc.dart';
import 'package:order_system_mobile_client/domain/bloc/field_forms/text_field_bloc.dart';
import 'package:order_system_mobile_client/domain/bloc/login_form/login_form_bloc.dart';
import 'package:order_system_mobile_client/internal/api_client.dart';
import 'package:order_system_models/order_system_models.dart';

class SignUpFormBloc extends Bloc<SignUpFormEvent, SignUpFormState> {
  final TextFieldBloc nameBloc;
  final TextFieldBloc phoneBloc;
  final TextFieldBloc addressBloc;
  final TextFieldBloc loginBloc;
  final PasswordFieldBloc passwordBloc;

  SignUpFormBloc(this.nameBloc, this.phoneBloc, this.addressBloc,
      this.loginBloc, this.passwordBloc)
      : super(InitSignUpFormState());

  @override
  Future<void> close() {
    nameBloc.close();
    phoneBloc.close();
    addressBloc.close();
    loginBloc.close();
    passwordBloc.close();
    return super.close();
  }

  @override
  Stream<SignUpFormState> mapEventToState(SignUpFormEvent event) async* {
    if (event is TrySignUpFormEvent) {
      yield await _mapSignUpToState(event);
    }
    if (event is UnFocusLoginFormEvent) {
      if (nameBloc.focusNode.hasFocus) {
        nameBloc.focusNode.unfocus();
      }
      if (phoneBloc.focusNode.hasFocus) {
        phoneBloc.focusNode.unfocus();
      }
      if (addressBloc.focusNode.hasFocus) {
        addressBloc.focusNode.unfocus();
      }
      if (loginBloc.focusNode.hasFocus) {
        loginBloc.focusNode.unfocus();
      }
      if (passwordBloc.focusNode.hasFocus) {
        passwordBloc.focusNode.unfocus();
      }
    }
  }

  Future<SignUpFormState> _mapSignUpToState(TrySignUpFormEvent event) async {
    ClientsClient clientsClient = ClientsClient(MobileApiClient());
    clientsClient
        .create(Client(
            name: nameBloc.value,
            phone: phoneBloc.value,
            address: addressBloc.value,
            login: loginBloc.value,
            password: passwordBloc.value))
        .then((createdClient) => SuccessSignUpFormState())
        .catchError((signUpError) {
      print('Sign up failed');
      print(signUpError);
      return FailedSignUpFormState();
    });
    return SuccessSignUpFormState();
  }
}

@immutable
abstract class SignUpFormEvent {}

class TrySignUpFormEvent extends SignUpFormEvent {}

class UnFocusSignUpFormEvent extends SignUpFormEvent {}

@immutable
abstract class SignUpFormState {}

class InitSignUpFormState extends SignUpFormState {}

class SuccessSignUpFormState extends SignUpFormState {}

class FailedSignUpFormState extends SignUpFormState {}
