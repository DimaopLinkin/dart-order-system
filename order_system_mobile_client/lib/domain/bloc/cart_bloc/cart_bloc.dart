import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_mobile_client/data/globals.dart' as globals;
import 'package:order_system_models/order_system_models.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartBloc(CartState initialState) : super(initialState);

  @override
  Stream<CartState> mapEventToState(CartEvent event) async* {
    if (event is IncrementCartEvent) {
      yield _mapIncrementToState(event);
    }
    if (event is DecrementCartEvent) {
      yield _mapDecrementToState(event);
    }
  }

  // TODO: Переделать без globals
  CartState _mapIncrementToState(IncrementCartEvent event) {
    globals.cartList.remove(event.dish);
    event.dish.count += 1;
    globals.cartList.add(event.dish);
    return CartState(globals.cartList);
  }

  CartState _mapDecrementToState(DecrementCartEvent event) {
    globals.cartList.remove(event.dish);
    event.dish.count -= 1;

    if (event.dish.count != 0) {
      globals.cartList.add(event.dish);
    }
    return CartState(globals.cartList);
  }
}

@immutable
class CartEvent {}

class IncrementCartEvent extends CartEvent {
  final InOrderDish dish;

  IncrementCartEvent(this.dish);
}

class DecrementCartEvent extends CartEvent {
  final dish;

  DecrementCartEvent(this.dish);
}

@immutable
class CartState {
  final listDishes;
  CartState(this.listDishes);
}
