import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_api_client/order_system_api_client.dart';
import 'package:order_system_mobile_client/internal/api_client.dart';
import 'package:order_system_models/order_system_models.dart';

class OrderBloc extends Bloc<OrderEvent, OrderState> {
  OrderBloc(OrderState initialState) : super(LoadingOrderState());

  @override
  Stream<OrderState> mapEventToState(OrderEvent event) async* {
    if (event is RefreshOrderEvent) {
      yield LoadingOrderState();
      yield await _mapRefreshToState(event);
    }
  }

  Future<OrderState> _mapRefreshToState(RefreshOrderEvent event) async {
    try {
      Order order = await OrdersIdClient(MobileApiClient()).read(event.orderId);
      return LoadedOrderState(order);
    } on Exception catch (e) {
      return ErrorOrderState(e);
    }
  }
}

@immutable
abstract class OrderEvent {}

class RefreshOrderEvent extends OrderEvent {
  final orderId;
  RefreshOrderEvent(this.orderId);
}

@immutable
abstract class OrderState {}

class LoadingOrderState extends OrderState {}

class LoadedOrderState extends OrderState {
  final Order order;
  LoadedOrderState(this.order);
}

class ErrorOrderState extends OrderState {
  final dynamic error;
  ErrorOrderState(this.error);
}
