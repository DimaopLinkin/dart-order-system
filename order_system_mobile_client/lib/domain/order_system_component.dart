library order_system_component;

import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:order_system_models/order_system_models.dart';
import 'package:flutter/widgets.dart';

typedef void NotificationCallback<T>(T valueObject);

class OrderSystemComponent {
  final String _address;
  WebSocket _webSocket;
  StreamSubscription _wsStreamSubscription;

  var _orderIdsWithUnreadUpdates = HashSet<OrderId>();
  var _orderStreamControllers = <_OrderStreamControllerWrapper>[];
  var _unreadOrderNotificationControllers = <StreamController<Set<OrderId>>>[];

  OrderSystemComponent(this._address);

  Future<void> connect() async {
    Future<WebSocket> ws = WebSocket.connect(_address);
    ws.then((webSocket) {
      _webSocket = webSocket;
      _wsStreamSubscription = webSocket.listen((data) {
        if (data is String) {
          final updatedOrder = Order.fromJson(json.decode(data));
          OrderId orderId = updatedOrder.id;
          bool orderConsumed = false;

          _orderStreamControllers.forEach((orderStreamControllerWrapper) {
            if (!orderStreamControllerWrapper._streamController.isClosed &&
                (orderStreamControllerWrapper._orderId == orderId)) {
              orderConsumed = true;
              orderStreamControllerWrapper._streamController.sink
                  .add(updatedOrder);
            }
          });

          if (!orderConsumed) {
            _orderIdsWithUnreadUpdates.add(orderId);
            _notifyUnread();
          }
        }
      })
        ..onError((err) {
          // reconnect
          print(err);
          connect();
        });
    });
  }

  StreamSubscription<Order> subscribeOrders(
      NotificationCallback<Order> callback, OrderId orderId) {
    _orderIdsWithUnreadUpdates.remove(orderId);
    _notifyUnread();
    var streamController = StreamController<Order>();
    _orderStreamControllers
        .add(_OrderStreamControllerWrapper(orderId, streamController));
    streamController.onCancel = () => streamController.close();
    return streamController.stream.listen((order) {
      callback(order);
    });
  }

  StreamSubscription<Set<OrderId>> subscribeUnreadUpdatesNotification(
      NotificationCallback<Set<OrderId>> callback) {
    var streamController = StreamController<Set<OrderId>>();
    _unreadOrderNotificationControllers.add(streamController);
    streamController.onCancel = () => streamController.close();
    var result = streamController.stream.listen((orderIdSet) {
      callback(orderIdSet);
    });
    streamController.sink.add(_orderIdsWithUnreadUpdates);
    return result;
  }

  void dispose() {
    _unreadOrderNotificationControllers.forEach((streamController) {
      if (!streamController.isClosed) {
        streamController.close();
      }
    });
    _unreadOrderNotificationControllers = null;

    _orderStreamControllers.forEach((streamControllerWrapper) {
      if (!streamControllerWrapper._streamController.isClosed) {
        streamControllerWrapper._streamController.close();
      }
    });
    _orderStreamControllers = null;

    _wsStreamSubscription?.cancel()?.then((_) {
      _webSocket?.close();
    });
  }

  void _notifyUnread() {
    _unreadOrderNotificationControllers
        .forEach((unreadOrderNotificationController) {
      if (!unreadOrderNotificationController.isClosed) {
        unreadOrderNotificationController.sink.add(_orderIdsWithUnreadUpdates);
      }
    });
  }
}

class _OrderStreamControllerWrapper {
  OrderId _orderId;
  StreamController<Order> _streamController;

  _OrderStreamControllerWrapper(this._orderId, this._streamController);
}

class OrderSystemComponentWidget extends InheritedWidget {
  final OrderSystemComponent orderSystemComponent;

  OrderSystemComponentWidget(this.orderSystemComponent, child)
      : super(child: child);

  static OrderSystemComponentWidget of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<OrderSystemComponentWidget>();
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
}
