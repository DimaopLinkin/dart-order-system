import 'package:flutter/material.dart';
import 'package:order_system_mobile_client/domain/bloc/field_forms/text_field_bloc.dart';

class NameWidget extends StatefulWidget {
  final String value;
  final TextFieldBloc nameBloc;

  NameWidget(this.value, this.nameBloc);

  @override
  _NameWidgetState createState() => _NameWidgetState(value);
}

class _NameWidgetState extends State<NameWidget> {

  final String value;

  final TextEditingController _loginController = TextEditingController();

  _NameWidgetState(this.value);

  String _validateLogin(String value) {
    if (value.length < 2) {
      // check login rules here
      return 'The Login must be at least 2 characters.';
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: _loginController,
      focusNode: widget.nameBloc.focusNode,
      onChanged: (name) => _onUpdateBlocName(name, widget.nameBloc),
      validator: this._validateLogin,
      decoration:
      InputDecoration(hintText: value, labelText: 'Enter your $value'),
    );
  }

  void _onUpdateBlocName(String name, TextFieldBloc nameBloc) {
    nameBloc.add(TextFieldUpdateEvent(name));
  }
}
