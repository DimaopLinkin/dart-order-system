import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  final String label;
  final Function onTap;

  const Button(this.label, this.onTap);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        SizedBox(
          width: 300,
          height: 80,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(45),
              color: Colors.deepPurple[400],
            ),
          ),
        ),
        FlatButton(
          child: Text(
            label,
            style: TextStyle(color: Colors.white, fontSize: 36),
          ),
          onPressed: onTap,
        ),
      ],
    );
  }
}
