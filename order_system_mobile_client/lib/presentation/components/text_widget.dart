import 'package:flutter/material.dart';

class TextWidget extends StatelessWidget {
  final label;
  final text;
  const TextWidget({
    Key key,
    @required this.label,
    @required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 60,
      child: Column(
        children: [
          SizedBox(
            width: double.infinity,
            height: 59,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    label,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                    ),
                  ),
                  Text(text),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 1,
            child: Container(
              color: Colors.deepPurple[400],
            ),
          )
        ],
      ),
    );
  }
}
