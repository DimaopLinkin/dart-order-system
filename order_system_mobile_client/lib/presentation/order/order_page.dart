import 'package:flutter/material.dart';
import 'package:order_system_mobile_client/domain/bloc/order/order_bloc.dart';
import 'package:order_system_mobile_client/presentation/order/list_order_information_widget.dart';
import 'package:order_system_models/order_system_models.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class OrderPage extends StatefulWidget {
  final Order order;

  const OrderPage({Key key, @required this.order}) : super(key: key);
  @override
  _OrderPageState createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Order information'),
        centerTitle: true,
      ),
      body: BlocProvider(
        create: (context) => OrderBloc(LoadingOrderState()),
        child: ListOrderInformation(orderId: widget.order.id),
      ),
    );
  }
}
