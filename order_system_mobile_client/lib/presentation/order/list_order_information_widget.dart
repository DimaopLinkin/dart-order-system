import 'package:flutter/material.dart';
import 'package:order_system_mobile_client/domain/bloc/order/order_bloc.dart';
import 'package:order_system_mobile_client/presentation/components/loading_widget.dart';
import 'package:order_system_mobile_client/presentation/components/text_widget.dart';
import 'package:order_system_models/order_system_models.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListOrderInformation extends StatefulWidget {
  final OrderId orderId;

  const ListOrderInformation({Key key, this.orderId}) : super(key: key);
  @override
  _ListOrderInformationState createState() => _ListOrderInformationState();
}

class _ListOrderInformationState extends State<ListOrderInformation> {
  @override
  void initState() {
    BlocProvider.of<OrderBloc>(context).add(RefreshOrderEvent(widget.orderId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<OrderBloc, OrderState>(
      listener: (context, state) {
        if (state is ErrorOrderState) {
          print(state.error);
        }
      },
      child: Column(
        children: [
          Expanded(
              child: RefreshIndicator(
            onRefresh: () async {
              BlocProvider.of<OrderBloc>(context)
                  .add(RefreshOrderEvent(widget.orderId));
            },
            child: BlocBuilder<OrderBloc, OrderState>(
              builder: (context, state) {
                if (state is LoadedOrderState) {
                  return ListView(
                    children: [
                      TextWidget(
                          label: 'Number', text: state.order.id.toString()),
                      TextWidget(label: 'Status', text: state.order.status),
                      TextWidget(
                          label: 'Opened',
                          text: state.order.createdAt.toString()),
                    ],
                  );
                } else {
                  return LoadingWidget();
                }
              },
            ),
          ))
        ],
      ),
    );
  }
}
