import 'package:flutter/material.dart';
import 'package:order_system_mobile_client/presentation/cart/cart_page.dart';

class CartActionWidget extends StatefulWidget {
  @override
  _CartActionWidgetState createState() => _CartActionWidgetState();
}

class _CartActionWidgetState extends State<CartActionWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.0),
              child: Icon(
                Icons.shopping_cart,
              ),
            )
          ],
        ),
      ),
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => CartPage()));
      },
    );
  }
}
