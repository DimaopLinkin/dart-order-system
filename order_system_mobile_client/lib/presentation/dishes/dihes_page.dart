import 'package:flutter/material.dart';
import 'package:order_system_mobile_client/domain/bloc/list_dishes_widget/list_dishes_widget_bloc.dart';
import 'package:order_system_mobile_client/presentation/cart_action/cart_action_widget.dart';
import 'package:order_system_mobile_client/presentation/menu_drawer/menu_drawer_widget.dart';
import 'package:order_system_models/order_system_models.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'list_dihes/list_dihes_widget.dart';

class DishesPage extends StatelessWidget {
  Restaurant restaurant;

  DishesPage({Key key, @required this.restaurant});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dishes'),
        centerTitle: true,
        actions: [
          CartActionWidget(),
        ],
      ),
      drawer: MenuDrawerWidget(),
      body: BlocProvider(
        create: (context) => ListDishesWidgetBloc(LoadingListDishesWidgetState),
        child: ListDishesWidget(
          restaurant: restaurant,
        ),
      ),
    );
  }
}
