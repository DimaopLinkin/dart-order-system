import 'package:flutter/material.dart';
import 'package:order_system_mobile_client/data/globals.dart' as globals;
import 'package:order_system_mobile_client/domain/bloc/dish_tile_widget/dish_tile_widget_bloc.dart';
import 'package:order_system_mobile_client/domain/bloc/list_dishes_widget/list_dishes_widget_bloc.dart';
import 'package:order_system_mobile_client/presentation/components/loading_widget.dart';
import 'package:order_system_models/order_system_models.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListDishesWidget extends StatefulWidget {
  ListDishesWidget({Key key, @required this.restaurant}) : super(key: key);
  final Restaurant restaurant;

  @override
  _ListDishesWidgetState createState() => _ListDishesWidgetState();
}

class _ListDishesWidgetState extends State<ListDishesWidget> {
  @override
  void initState() {
    globals.currentRestaurant = widget.restaurant;
    BlocProvider.of<ListDishesWidgetBloc>(context)
        .add(RefreshListDishesWidgetEvent(widget.restaurant.id));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ListDishesWidgetBloc, ListDishesWidgetState>(
      listener: (context, state) {
        if (state is ErrorListDishesWidgetState) {
          print(state.error);
        }
      },
      child: Column(
        children: [
          Expanded(
              child: RefreshIndicator(
            onRefresh: () async {
              BlocProvider.of<ListDishesWidgetBloc>(context)
                  .add(RefreshListDishesWidgetEvent(widget.restaurant.id));
            },
            child: BlocBuilder<ListDishesWidgetBloc, ListDishesWidgetState>(
              builder: (context, state) {
                if (state is LoadedListDishesWidgetState) {
                  return ListView.builder(
                    itemCount: state.listDishes.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        child: _buildListTile(state.listDishes[index]),
                      );
                    },
                  );
                } else {
                  return LoadingWidget();
                }
              },
            ),
          ))
        ],
      ),
    );
  }

  ListTile _buildListTile(Dish dish) {
    return ListTile(
      title: SizedBox(
        height: 100,
        child: Column(
          children: [
            SizedBox(
              height: 99,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        width: 70,
                        height: 70,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(60),
                          color: Colors.grey[300],
                        ),
                      ),
                      Text(
                        'Avatar',
                        style: TextStyle(fontSize: 12),
                      ),
                    ],
                  ),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            dish.name,
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            dish.information,
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      BlocProvider<DishTileWidgetCubit>(
                        create: (context) => DishTileWidgetCubit(dish),
                        child: BlocBuilder<DishTileWidgetCubit,
                            DishTileWidgetState>(
                          builder: (context, state) {
                            if (state.counterValue == 0) {
                              return IconButton(
                                icon: Icon(
                                  Icons.add_shopping_cart,
                                ),
                                onPressed: () =>
                                    BlocProvider.of<DishTileWidgetCubit>(
                                            context)
                                        .increment(),
                              );
                            }
                            return Row(
                              children: [
                                IconButton(
                                  icon: Icon(Icons.remove),
                                  onPressed: () =>
                                      BlocProvider.of<DishTileWidgetCubit>(
                                              context)
                                          .decrement(),
                                ),
                                Text(state.counterValue.toString()),
                                IconButton(
                                  icon: Icon(Icons.add),
                                  onPressed: () =>
                                      BlocProvider.of<DishTileWidgetCubit>(
                                              context)
                                          .increment(),
                                ),
                              ],
                            );
                          },
                        ),
                      ),
                      Text(
                        dish.price.toString(),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 1,
              width: double.infinity,
              child: Container(
                color: Colors.deepPurple[400],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
