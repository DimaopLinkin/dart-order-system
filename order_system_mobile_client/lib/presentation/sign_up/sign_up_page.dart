import 'package:flutter/material.dart';
import 'package:order_system_mobile_client/domain/bloc/sign_up_form/sign_up_form_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_mobile_client/presentation/components/button_widget.dart';
import 'package:order_system_mobile_client/presentation/components/form/name_widget.dart';
import 'package:order_system_mobile_client/presentation/components/form/password_widget.dart';

class SignUpPage extends StatefulWidget {
  SignUpPage({Key key}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocListener<SignUpFormBloc, SignUpFormState>(
      listener: (context, state) {
        if (state is SuccessSignUpFormState) {
          _onSignUpSuccess(context);
        }
        if (state is FailedSignUpFormState) {
          _onSignUpFailed(context);
        }
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text('Sign Up'),
          centerTitle: true,
        ),
        body: GestureDetector(
          onTap: () {
            BlocProvider.of<SignUpFormBloc>(context)
                .add(UnFocusSignUpFormEvent());
          },
          child: BlocBuilder<SignUpFormBloc, SignUpFormState>(
            builder: (context, signUpFormState) {
              return Container(
                padding: EdgeInsets.all(20.0),
                child: Form(
                  key: this._formKey,
                  child: Column(
                    children: [
                      NameWidget('Name',
                          BlocProvider.of<SignUpFormBloc>(context).nameBloc),
                      NameWidget('Phone',
                          BlocProvider.of<SignUpFormBloc>(context).phoneBloc),
                      NameWidget('Address',
                          BlocProvider.of<SignUpFormBloc>(context).addressBloc),
                      NameWidget('Login',
                          BlocProvider.of<SignUpFormBloc>(context).loginBloc),
                      PasswordWidget(BlocProvider.of<SignUpFormBloc>(context)
                          .passwordBloc),
                      SizedBox(
                        height: 40,
                      ),
                      Button('Sign up', _onSignUp),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  void _onSignUp() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    BlocProvider.of<SignUpFormBloc>(context).add(TrySignUpFormEvent());
  }

  void _onSignUpSuccess(BuildContext context) {
    final snackBar = SnackBar(content: Text('Sign up success'));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  void _onSignUpFailed(BuildContext context) {
    final snackBar = SnackBar(content: Text('Sign up failed'));
    Scaffold.of(context).showSnackBar(snackBar);
  }
}
