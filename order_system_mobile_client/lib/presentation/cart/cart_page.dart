import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_mobile_client/data/globals.dart' as globals;
import 'package:order_system_mobile_client/domain/bloc/cart_bloc/cart_bloc.dart';
import 'package:order_system_mobile_client/presentation/cart/list_cart_items/list_cart_items_widget.dart';
import 'package:order_system_mobile_client/presentation/components/button_widget.dart';
import 'package:order_system_mobile_client/presentation/create_order/create_order_page.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CartBloc(CartState(globals.cartList)),
      child: Scaffold(
        appBar: AppBar(
          title: Text('Cart'),
          centerTitle: true,
        ),
        body: BlocBuilder<CartBloc, CartState>(
          builder: (context, state) {
            return Stack(
              children: [
                BlocBuilder<CartBloc, CartState>(
                  builder: (context, state) {
                    return ListCartItems();
                  },
                ),
                buildCreateOrderButton(
                    BlocProvider.of<CartBloc>(context).state.listDishes),
              ],
            );
          },
        ),
      ),
    );
  }

  Padding buildCreateOrderButton(List listDishes) {
    if (listDishes.isNotEmpty) {
      return Padding(
        padding: const EdgeInsets.only(bottom: 25.0),
        child: Container(
          alignment: Alignment.bottomCenter,
          child: Button('Create Order', () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => CreateOrderPage()));
          }),
        ),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text('Cart is empty'),
      );
    }
  }
}
