import 'package:flutter/material.dart';
import 'package:order_system_mobile_client/domain/bloc/cart_bloc/cart_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_models/order_system_models.dart';

class ListCartItems extends StatefulWidget {
  @override
  _ListCartItemsState createState() => _ListCartItemsState();
}

class _ListCartItemsState extends State<ListCartItems> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CartBloc, CartState>(
      builder: (context, state) {
        return ListView.builder(
          itemCount: state.listDishes.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              child: _buildListTile(state.listDishes[index]),
            );
          },
        );
      },
    );
  }

  ListTile _buildListTile(InOrderDish inOrderDish) {
    return ListTile(
      title: SizedBox(
        height: 100,
        child: Column(
          children: [
            SizedBox(
              height: 99,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        width: 70,
                        height: 70,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(60),
                          color: Colors.grey[300],
                        ),
                      ),
                      Text(
                        'Avatar',
                        style: TextStyle(fontSize: 12),
                      ),
                    ],
                  ),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            inOrderDish.dish.name,
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            inOrderDish.dish.information,
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      BlocBuilder<CartBloc, CartState>(
                        builder: (context, state) {
                          return Row(
                            children: [
                              IconButton(
                                icon: Icon(Icons.remove),
                                onPressed: () =>
                                    BlocProvider.of<CartBloc>(context)
                                        .add(DecrementCartEvent(inOrderDish)),
                              ),
                              Text(inOrderDish.count.toString()),
                              IconButton(
                                icon: Icon(Icons.add),
                                onPressed: () =>
                                    BlocProvider.of<CartBloc>(context)
                                        .add(IncrementCartEvent(inOrderDish)),
                              ),
                            ],
                          );
                        },
                      ),
                      Text(
                        inOrderDish.dish.price.toString(),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 1,
              width: double.infinity,
              child: Container(
                color: Colors.deepPurple[400],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
