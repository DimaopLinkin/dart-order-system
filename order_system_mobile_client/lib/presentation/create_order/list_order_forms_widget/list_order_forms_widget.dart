import 'package:flutter/material.dart';
import 'package:order_system_mobile_client/data/globals.dart' as globals;
import 'package:order_system_mobile_client/domain/bloc/create_order/create_order_bloc.dart';
import 'package:order_system_mobile_client/presentation/components/button_widget.dart';
import 'package:order_system_mobile_client/presentation/components/text_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_mobile_client/presentation/order/order_page.dart';
import 'package:order_system_models/order_system_models.dart';

class ListOrderForms extends StatefulWidget {
  @override
  _ListOrderFormsState createState() => _ListOrderFormsState();
}

class _ListOrderFormsState extends State<ListOrderForms> {
  @override
  Widget build(BuildContext context) {
    return BlocListener<CreateOrderBloc, CreateOrderState>(
      listener: (context, state) {
        if (state is SubmitSuccessCreateOrderState) {
          _onSubmitSuccess(state.order);
        }
        if (state is SubmitFailedCreateOrderState) {
          _onSubmitFailed();
        }
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              TextWidget(label: 'Address', text: globals.currentUser.address),
              TextWidget(label: 'Name', text: globals.currentUser.name),
              TextWidget(label: 'Phone', text: globals.currentUser.phone),
            ],
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 15, 15, 15),
            child: Button('Submit', () {
              _onSubmit();
            }),
          )
        ],
      ),
    );
  }

  void _onSubmit() {
    BlocProvider.of<CreateOrderBloc>(context).add(TrySubmitCreateOrderEvent());
  }

  void _onSubmitSuccess(Order order) {
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (context) => OrderPage(order: order)));
  }

  void _onSubmitFailed() {
    final snackBar = SnackBar(content: Text('Submit failed'));
    Scaffold.of(context).showSnackBar(snackBar);
  }
}
