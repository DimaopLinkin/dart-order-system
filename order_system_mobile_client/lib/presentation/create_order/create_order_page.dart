import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_mobile_client/domain/bloc/create_order/create_order_bloc.dart';
import 'package:order_system_mobile_client/presentation/create_order/list_order_forms_widget/list_order_forms_widget.dart';

class CreateOrderPage extends StatefulWidget {
  @override
  _CreateOrderPageState createState() => _CreateOrderPageState();
}

class _CreateOrderPageState extends State<CreateOrderPage> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CreateOrderBloc(InitCreateOrderState()),
      child: Scaffold(
        appBar: AppBar(
          title: Text('Order'),
          centerTitle: true,
        ),
        body: ListOrderForms(),
      ),
    );
  }
}
