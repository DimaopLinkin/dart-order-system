import 'package:flutter/material.dart';
import 'package:order_system_mobile_client/presentation/cart_action/cart_action_widget.dart';
import 'package:order_system_mobile_client/presentation/menu_drawer/menu_drawer_widget.dart';

import 'list_restaurants/list_restaurants_widget.dart';

class RestaurantsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Restaurants'),
        centerTitle: true,
        actions: [
          CartActionWidget(),
        ],
      ),
      drawer: MenuDrawerWidget(),
      body: ListRestaurantsWidget(),
    );
  }
}
