import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:order_system_mobile_client/presentation/dishes/dihes_page.dart';
import 'package:order_system_mobile_client/presentation/components/restaurant_card_widget.dart';
import 'package:order_system_models/order_system_models.dart';
import 'package:order_system_mobile_client/domain/bloc/list_restaurants_widget/list_restaurants_widget_bloc.dart';
import 'package:order_system_mobile_client/presentation/components/loading_widget.dart';

class ListRestaurantsWidget extends StatefulWidget {
  @override
  _ListRestaurantsWidgetState createState() => _ListRestaurantsWidgetState();
}

class _ListRestaurantsWidgetState extends State<ListRestaurantsWidget> {
  @override
  void initState() {
    BlocProvider.of<ListRestaurantsWidgetBloc>(context)
        .add(RefreshListRestaurantsWidgetEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ListRestaurantsWidgetBloc, ListRestaurantsWidgetState>(
      listener: (context, state) {
        if (state is ErrorListRestaurantsWidgetState) {
          print(state.error);
        }
      },
      child: Column(
        children: [
          Expanded(
              child: RefreshIndicator(
            onRefresh: () async {
              BlocProvider.of<ListRestaurantsWidgetBloc>(context)
                  .add(RefreshListRestaurantsWidgetEvent());
            },
            child: BlocBuilder<ListRestaurantsWidgetBloc,
                ListRestaurantsWidgetState>(
              builder: (context, state) {
                if (state is LoadedListRestaurantsWidgetState) {
                  return ListView.builder(
                    itemCount: state.listRestaurants.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        child: _buildListTile(state.listRestaurants[index]),
                      );
                    },
                  );
                } else {
                  return LoadingWidget();
                }
              },
            ),
          ))
        ],
      ),
    );
  }

  ListTile _buildListTile(Restaurant restaurant) {
    return ListTile(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return DishesPage(restaurant: restaurant);
        }));
      },
      title: RestaurantCardWidget(
        name: restaurant.name,
        info: restaurant.information,
      ),
    );
  }
}
