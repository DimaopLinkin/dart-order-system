import 'package:test/test.dart';
import 'package:order_system_models/order_system_models.dart';

void main() {
  group('Equatable', () {
    test('Category', () {
      Category cat1 = Category(name: "Горячее");
      Category cat2 = Category.fromJson({"name": "Горячее"});
      expect(cat1, equals(cat2));
      var out1 = cat1.json;
      expect(out1, equals({"name": "Горячее"}));
      var out2 = cat2.json;
      expect(out2, equals({"name": "Горячее"}));
    });
    test('Dish', () {
      Dish dish1 = Dish(
          id: DishId("8552"),
          name: "Окорок",
          information: "Куриный",
          price: 150,
          restaurant: RestaurantId("123321"),
          categories: [
            Category(name: "Горячее"),
            Category(name: "Суши"),
            Category(name: "Роллы")
          ]);
      Dish dish2 = Dish.fromJson({
        "id": "8552",
        "name": "Окорок",
        "information": "Куриный",
        "price": 150,
        "restaurant": "123321",
        "categories": [
          {"name": "Горячее"},
          {"name": "Суши"},
          {"name": "Роллы"}
        ]
      });
      expect(dish1, equals(dish2));
      var out1 = dish1.json;
      expect(
          out1,
          equals({
            "id": "8552",
            "name": "Окорок",
            "information": "Куриный",
            "price": 150,
            "restaurant": "123321",
            "categories": [
              {"name": "Горячее"},
              {"name": "Суши"},
              {"name": "Роллы"}
            ]
          }));
      var out2 = dish2.json;
      expect(
          out2,
          equals({
            "id": "8552",
            "name": "Окорок",
            "information": "Куриный",
            "price": 150,
            "restaurant": "123321",
            "categories": [
              {"name": "Горячее"},
              {"name": "Суши"},
              {"name": "Роллы"}
            ]
          }));
    });
    // Произведена замена в данной модели DishId на Dish
    //
    // test('In_Order_Dish', () {
    //   InOrderDish orderDish1 = InOrderDish(dish: DishId("8552"), count: 2);
    //   InOrderDish orderDish2 =
    //       InOrderDish.fromJson({"dish": "8552", "count": 2});
    //   expect(orderDish1, equals(orderDish2));
    //   var out1 = orderDish1.json;
    //   expect(out1, equals({"dish": "8552", "count": 2}));
    //   var out2 = orderDish2.json;
    //   expect(out2, equals({"dish": "8552", "count": 2}));
    // });
    test('Courier', () {
      Courier cour1 = Courier(
          id: CourierId("2556"),
          name: "Володя",
          phone: "79998887766",
          login: "Volodya7766",
          password: "VolodyaRullezz");
      Courier cour2 = Courier.fromJson({
        "id": "2556",
        "name": "Володя",
        "phone": "79998887766",
        "login": "Volodya7766",
        "password": "VolodyaRullezz"
      });
      expect(cour1, equals(cour2));
      var out1 = cour1.json;
      expect(
          out1,
          equals({
            "id": "2556",
            "name": "Володя",
            "phone": "79998887766",
            "login": "Volodya7766",
            "password": "VolodyaRullezz"
          }));
      var out2 = cour2.json;
      expect(
          out2,
          equals({
            "id": "2556",
            "name": "Володя",
            "phone": "79998887766",
            "login": "Volodya7766",
            "password": "VolodyaRullezz"
          }));
    });
    test('Client', () {
      Client client1 = Client(
          id: ClientId("999666"),
          name: "Артем",
          phone: "71112223344",
          address: "пр. Строителей, д. 25, кв. 32",
          login: "Artem44",
          password: "NasNeDogonyat");
      Client client2 = Client.fromJson({
        "id": "999666",
        "name": "Артем",
        "phone": "71112223344",
        "address": "пр. Строителей, д. 25, кв. 32",
        "login": "Artem44",
        "password": "NasNeDogonyat"
      });
      expect(client1, equals(client2));
      var out1 = client1.json;
      expect(
          out1,
          equals({
            "id": "999666",
            "name": "Артем",
            "phone": "71112223344",
            "address": "пр. Строителей, д. 25, кв. 32",
            "login": "Artem44",
            "password": "NasNeDogonyat"
          }));
      var out2 = client2.json;
      expect(
          out2,
          equals({
            "id": "999666",
            "name": "Артем",
            "phone": "71112223344",
            "address": "пр. Строителей, д. 25, кв. 32",
            "login": "Artem44",
            "password": "NasNeDogonyat"
          }));
    });
    test('Restaurant', () {
      Restaurant rest1 = Restaurant(
        id: RestaurantId("123321"),
        name: "Макдональдс",
        information: "Бургерная из Америки",
        address: "пр. Ленинский, д. 1",
        login: "MacDonalds",
        password: "HeyYou123",
      );
      Restaurant rest2 = Restaurant.fromJson({
        "id": "123321",
        "name": "Макдональдс",
        "information": "Бургерная из Америки",
        "address": "пр. Ленинский, д. 1",
        "login": "MacDonalds",
        "password": "HeyYou123"
      });
      expect(rest1, equals(rest2));
      var out1 = rest1.json;
      expect(
          out1,
          equals({
            "id": "123321",
            "name": "Макдональдс",
            "information": "Бургерная из Америки",
            "address": "пр. Ленинский, д. 1",
            "login": "MacDonalds",
            "password": "HeyYou123"
          }));
      var out2 = rest2.json;
      expect(
          out2,
          equals({
            "id": "123321",
            "name": "Макдональдс",
            "information": "Бургерная из Америки",
            "address": "пр. Ленинский, д. 1",
            "login": "MacDonalds",
            "password": "HeyYou123"
          }));
    });
    // Произведена замена в данной модели ClientId, RestaurantId, CourierId
    // на Client, Restaurant, Courier
    //
    // test('Order', () {
    //   Order order1 = Order(
    //       id: OrderId("2"),
    //       client: ClientId("999666"),
    //       restaurant: RestaurantId("123321"),
    //       courier: CourierId("2556"),
    //       status: "Готовится",
    //       dishList: [
    //         InOrderDish(dish: DishId("8552"), count: 2),
    //         InOrderDish(dish: DishId("8553"), count: 4)
    //       ],
    //       price: 1100,
    //       createdAt: DateTime.utc(2020, 11, 18, 3, 30),
    //       updatedAt: DateTime.utc(2020, 11, 18, 4, 30));
    //   Order order2 = Order.fromJson({
    //     "id": "2",
    //     "client": "999666",
    //     "restaurant": "123321",
    //     "courier": "2556",
    //     "status": "Готовится",
    //     "dishList": [
    //       {"dish": "8552", "count": 2},
    //       {"dish": "8553", "count": 4}
    //     ],
    //     "price": 1100,
    //     "createdAt": "2020-11-18 03:30Z",
    //     "updatedAt": "2020-11-18 04:30Z"
    //   });
    //   // expect(order1, equals(order2));
    //   expect(order1.json, equals(order2.json));
    // });
  });
}
