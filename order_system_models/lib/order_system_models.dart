/// Order system models
///
/// -
/// -
/// -
/// -
/// -
/// -
/// -
/// -
library order_system_models;

export 'src/category.dart';
export 'src/client.dart';
export 'src/courier.dart';
export 'src/dish.dart';
export 'src/in_order_dish.dart';
export 'src/order.dart';
export 'src/restaurant.dart';
