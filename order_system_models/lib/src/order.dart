import 'package:data_model/data_model.dart';
import 'package:equatable/equatable.dart';

import 'client.dart';
import 'courier.dart';
import 'in_order_dish.dart';
import 'restaurant.dart';

class Order with EquatableMixin implements Model<OrderId> {
  OrderId id;
  Client client;
  Restaurant restaurant;
  Courier courier;
  String status;
  List<InOrderDish> dishList;
  int price;
  final DateTime createdAt;
  DateTime updatedAt;
  Order(
      {this.id,
      this.client,
      this.restaurant,
      this.courier,
      this.status,
      this.dishList,
      this.price,
      this.createdAt,
      this.updatedAt});
  factory Order.fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    return Order(
        id: OrderId(json['id']),
        client: Client.fromJson(json['client']),
        restaurant: Restaurant.fromJson(json['restaurant']),
        courier: Courier.fromJson(json['courier']),
        status: json['status'],
        dishList: json['dishList'] is List
            ? List.from(json['dishList']
                .map((dishesJson) => InOrderDish.fromJson(dishesJson)))
            : null,
        price: json['price'],
        createdAt: json['createdAt'] is DateTime
            ? json['createdAt'].toUtc()
            : DateTime.parse(json['createdAt']).toUtc(),
        updatedAt: json['updatedAt'] is DateTime
            ? json['updatedAt'].toUtc()
            : DateTime.parse(json['updatedAt']).toUtc());
  }

  int get sum {
    var sum = 0;
    dishList.forEach((element) {
      sum += element.count * element.dish.price;
    });
    return sum;
  }

  Map<String, dynamic> get json => {
        'id': id?.json,
        'client': client?.json,
        'restaurant': restaurant?.json,
        'courier': courier?.json,
        'status': status,
        'dishList': dishList?.map((InOrderDish) => InOrderDish.json)?.toList(),
        'price': price,
        'createdAt': createdAt?.toUtc(),
        'updatedAt': updatedAt?.toUtc()
      }..removeWhere((key, value) => value == null);

  @override
  List<Object> get props => [
        id,
        client,
        restaurant,
        courier,
        status,
        dishList,
        price,
        createdAt,
        updatedAt
      ];
}

class OrderId extends ObjectId {
  OrderId._(id) : super(id);
  factory OrderId(id) {
    if (id == null) return null;
    return OrderId._(id);
  }
}
