import 'package:data_model/data_model.dart';
import 'package:equatable/equatable.dart';

class Courier with EquatableMixin implements Model<CourierId> {
  CourierId id;
  String name;
  String phone;
  String login;
  String password;
  Courier({this.id, this.name, this.phone, this.login, this.password});
  factory Courier.fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    return Courier(
        id: CourierId(json['id']),
        name: json['name'],
        phone: json['phone'],
        login: json['login'],
        password: json['password']);
  }

  Map<String, dynamic> get json => {
        'id': id?.json,
        'name': name,
        'phone': phone,
        'login': login,
        'password': password
      }..removeWhere((key, value) => value == null);

  @override
  List<Object> get props => [id, name, phone, login, password];
}

class CourierId extends ObjectId {
  CourierId._(id) : super(id);
  factory CourierId(id) {
    if (id == null) return null;
    return CourierId._(id);
  }
}
