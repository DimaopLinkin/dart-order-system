import 'package:data_model/data_model.dart';
import 'package:equatable/equatable.dart';

class Restaurant with EquatableMixin implements Model<RestaurantId> {
  RestaurantId id;
  String name;
  String information;
  String address;
  String login;
  String password;
  Restaurant(
      {this.id,
      this.name,
      this.information,
      this.address,
      this.login,
      this.password});
  factory Restaurant.fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    return Restaurant(
        id: RestaurantId(json['id']),
        name: json['name'],
        information: json['information'],
        address: json['address'],
        login: json['login'],
        password: json['password']);
  }

  Map<String, dynamic> get json => {
        'id': id?.json,
        'name': name,
        'information': information,
        'address': address,
        'login': login,
        'password': password
      }..removeWhere((key, value) => value == null);

  @override
  List<Object> get props => [id, name, information, address, login, password];
}

class RestaurantId extends ObjectId {
  RestaurantId._(id) : super(id);
  factory RestaurantId(id) {
    if (id == null) return null;
    return RestaurantId._(id);
  }
}
