import 'package:data_model/data_model.dart';
import 'package:equatable/equatable.dart';

class Client with EquatableMixin implements Model<ClientId> {
  ClientId id;
  String name;
  String phone;
  String address;
  String login;
  String password;
  Client(
      {this.id,
      this.name,
      this.phone,
      this.address,
      this.login,
      this.password});
  factory Client.fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    return Client(
        id: ClientId(json['id']),
        name: json['name'],
        phone: json['phone'],
        address: json['address'],
        login: json['login'],
        password: json['password']);
  }

  Map<String, dynamic> get json => {
        'id': id?.json,
        'name': name,
        'phone': phone,
        'address': address,
        'login': login,
        'password': password
      }..removeWhere((key, value) => value == null);

  @override
  List<Object> get props => [id, name, phone, address, login, password];
}

class ClientId extends ObjectId {
  ClientId._(id) : super(id);
  factory ClientId(id) {
    if (id == null) return null;
    return ClientId._(id);
  }
}
