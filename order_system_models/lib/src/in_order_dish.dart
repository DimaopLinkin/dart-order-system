import 'package:data_model/data_model.dart';
import 'package:equatable/equatable.dart';

import 'dish.dart';

class InOrderDish with EquatableMixin implements JsonEncodable {
  Dish dish;
  int count;
  InOrderDish({this.dish, this.count});
  factory InOrderDish.fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    return InOrderDish(dish: Dish.fromJson(json['dish']), count: json['count']);
  }

  Map<String, dynamic> get json => {
        'dish': dish?.json,
        'count': count,
      };

  @override
  List<Object> get props => [dish, count];
}
