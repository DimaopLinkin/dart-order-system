import 'package:data_model/data_model.dart';
import 'package:equatable/equatable.dart';
import 'package:order_system_models/order_system_models.dart';

import 'category.dart';

class Dish with EquatableMixin implements Model<DishId> {
  DishId id;
  String name;
  String information;
  int price;
  RestaurantId restaurant;
  List<Category> categories;
  Dish(
      {this.id,
      this.name,
      this.information,
      this.price,
      this.restaurant,
      this.categories});
  factory Dish.fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    return Dish(
        id: DishId(json['id']),
        name: json['name'],
        information: json['information'],
        price: json['price'],
        restaurant: RestaurantId(json['restaurant']),
        categories: json['categories'] is List
            ? List.from(json['categories']
                .map((categoryJson) => Category.fromJson(categoryJson)))
            : null);
  }

  Map<String, dynamic> get json => {
        'id': id?.json,
        'name': name,
        'information': information,
        'price': price,
        'restaurant': restaurant?.json,
        'categories': categories?.map((category) => category.json)?.toList()
      };

  @override
  List<Object> get props =>
      [id, name, information, price, restaurant, categories];
}

class DishId extends ObjectId {
  DishId._(id) : super(id);
  factory DishId(id) {
    if (id == null) return null;
    return DishId._(id);
  }
}
