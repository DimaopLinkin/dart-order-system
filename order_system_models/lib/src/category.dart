import 'package:data_model/data_model.dart';
import 'package:equatable/equatable.dart';

class Category with EquatableMixin implements JsonEncodable {
  String name;
  Category({this.name});
  factory Category.fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    return Category(name: json['name']);
  }

  Map<String, dynamic> get json => {'name': name};

  @override
  List<Object> get props => [name];
}
