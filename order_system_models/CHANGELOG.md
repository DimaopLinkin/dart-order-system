# 0.0.3

## Some fixes
* All models now is Equatable
## Added Tests

# 0.0.2

## Fixed data model

* address now is String
* dish price now is int
* category now is enum
* added status enum
## order 
* client, restaurant, courier to client_id, restaurant_id, courier_id
* price now is int
## added updatedAt

# 0.0.1

## Initial version